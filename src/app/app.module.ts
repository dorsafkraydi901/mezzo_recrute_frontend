import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ProfileComponent } from './profile/profile.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AcceuilComponent } from './acceuil/acceuil.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { MenuComponent } from './header/menu/menu.component';
import { LoginComponent } from './login/login.component';
import { RegistreComponent } from './registre/registre.component';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { HeaderProfilComponent } from './header/header-profil/header-profil.component';
import { EditpComponent } from './editp/editp.component';
import { AddoffreComponent } from './_offre/addoffre/addoffre.component';
import { ShowComponent } from './_offre/show/show.component';
import { LoginEmpComponent } from './login-emp/login-emp.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { EditoComponent } from './_offre/edito/edito.component';
import { ShowcandidatureComponent } from './_candidature/showcandidature/showcandidature.component';
import { AddcandidatureComponent } from './_candidature/addcandidature/addcandidature.component';
import { DetailoffreComponent } from './_offre/detailoffre/detailoffre.component';
import { AddDemandeComponent } from './demande/add-demande/add-demande.component';
import { ErrorComponent } from './error/error.component';
import { DemandeDetailsComponent } from './demande/admin/demande-details/demande-details.component';
import { DemandeListComponent } from './demande/demande-list/demande-list.component';
import { DemandesComponent } from './demande/demandes/demandes.component';
import { DemandedetailComponent } from './demande/demandedetail/demandedetail.component';
import { DemanValideComponent } from './demande/deman-valide/deman-valide.component';
import { DdemandeComponent } from './_offre/ddemande/ddemande.component';
import { DemandeheaderComponent } from './header/demandeheader/demandeheader.component';
import { DvheaderComponent } from './header/dvheader/dvheader.component';
import { MatTabsModule } from '@angular/material/tabs';
import { ListeCandidatsComponent } from './_offre/liste-candidats/liste-candidats.component';
import { CandidateurdetailComponent } from './_offre/liste-candidats/candidateurdetail/candidateurdetail.component';
import { AdduseComponent } from './adduse/adduse.component';
import { ToastrModule } from 'ngx-toastr';
import { NgGoogleOneTapModule } from 'ng-google-one-tap';
import { SocialLoginModule, SocialAuthServiceConfig  ,  GoogleLoginProvider,FacebookLoginProvider} from 'angularx-social-login';
import { SpinnerComponent } from './spinner/spinner.component';
import { GetallComponent } from './news/getall/getall.component';
import { CreatnewsComponent } from './news/creatnews/creatnews.component';
import { DetailsComponent } from './news/details/details.component';
import { UpdatenewsComponent } from './news/updatenews/updatenews.component';
import { CandidatureAccepteComponent } from './candidature-accepte/candidature-accepte.component';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { ContactComponent } from './contact/contact.component';
import { GoogleMapsModule } from '@angular/google-maps';
import { ListeSpontaneComponent } from './liste-spontane/liste-spontane.component';

import { UpdateDemandeComponent } from './demande/update-demande/update-demande.component';
import { EmailComponent } from './email/email.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { CahngePasswordComponent } from './cahnge-password/cahnge-password.component';



@NgModule({
  declarations: [
    AppComponent,
    ProfileComponent,
    AcceuilComponent,
    HeaderComponent,
    FooterComponent,
    MenuComponent,
    LoginComponent,
    RegistreComponent,
    HeaderProfilComponent,
    EditpComponent,
    LoginEmpComponent,
      AddoffreComponent,
      ShowComponent,
      EditoComponent,
      ShowcandidatureComponent,
      AddcandidatureComponent,
      DetailoffreComponent,
      AddDemandeComponent,
      ErrorComponent,
      DemandeDetailsComponent,
      DemandeListComponent,
      DemandesComponent,
      DemandedetailComponent,
      DemanValideComponent,
      DdemandeComponent,
      DemandeheaderComponent,
      DvheaderComponent,
      ListeCandidatsComponent,
      CandidateurdetailComponent,
      AdduseComponent,
      SpinnerComponent,
      GetallComponent,
      CreatnewsComponent,
      DetailsComponent,
      UpdatenewsComponent,

      CandidatureAccepteComponent,
      ContactComponent,
      ListeSpontaneComponent,

      UpdateDemandeComponent,
      ResetPasswordComponent,
      EmailComponent,
      CahngePasswordComponent

      


     
 

  ],
  imports: [
    ReactiveFormsModule,
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    MatTabsModule,
    HttpClientModule,
    NgbModule ,
    RouterModule,
    SocialLoginModule,
    MatDialogModule,
    MatInputModule,
    MatButtonModule,
    SlickCarouselModule,
    ToastrModule.forRoot(),
    GoogleMapsModule,
    NgGoogleOneTapModule.config(
      {  //Look options table for some more avaialbe options and config here.
          client_id: '1012836209606-1eandke4od6kqvnjr4iaqt81n7snide8.apps.googleusercontent.com',
          cancel_on_tap_outside: false,
          authvalidate_by_googleapis: false,
          auto_select: false,
          disable_exponential_cooldowntime: false,
          context: 'signup',
  
      })
  ],
  providers: [
    {
      provide: 'SocialAuthServiceConfig',
      useValue: {
        autoLogin: false,
        providers: [
          {
            id: GoogleLoginProvider.PROVIDER_ID,
            provider: new GoogleLoginProvider(
              '115572091174-fkldfanudglumfqon5stvl2siuhfat57.apps.googleusercontent.com'
            )
          },
        
        ],
        onError: (err) => {
          console.error(err);
        }
      } as SocialAuthServiceConfig,
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }