import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PhotoserviceService {
  private apiEndpoint = 'http://localhost:8080/api/test/image';
  private apiEndpointCV = 'http://localhost:8080/api/test/file';
  constructor(private http: HttpClient) { }

  uploadImage(userId: string, formData: FormData) {
    const url = `${this.apiEndpoint}/id=${userId}`;
    return this.http.post(url, formData);
  }
  uploadImageoffre(offreId: string, formData: FormData) {
    const url = `${this.apiEndpoint}/idd=${offreId}`;
    return this.http.post(url, formData);
  }
  uploadImagenews(idnews: string, formData: FormData) {
    const url = `${this.apiEndpoint}/idnews=${idnews}`;
    return this.http.post(url, formData);
  }
  uploadCv(userId: string, formData: FormData) {
    const url = `${this.apiEndpointCV}/id=${userId}`;
    return this.http.post(url, formData);
  }

  getImage(id: string): Observable<any> {
    const url = `${this.apiEndpoint}/${id}`;
    return this.http.get(url);
  } 
}
