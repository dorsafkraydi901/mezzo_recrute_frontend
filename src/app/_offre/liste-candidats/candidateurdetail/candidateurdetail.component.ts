import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Candidature } from 'src/app/_serviceCandidature/Candidature';
import { CandidatureService } from 'src/app/_serviceCandidature/candidature.service';
import { StorageService } from 'src/app/_servicesUser/storage.service';
import { UserService } from 'src/app/_servicesUser/user.service';
import { Utilisateur } from 'src/app/_servicesUser/utilisateur';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-candidateurdetail',
  templateUrl: './candidateurdetail.component.html',
  styleUrls: ['./candidateurdetail.component.css']
})
export class CandidateurdetailComponent implements OnInit {
  candidature: Candidature | undefined;
  showSpinner: boolean = false;
  isSignUpFailed = false
  isLoggedIn = false;
  utilisateur : Utilisateur= new Utilisateur();
  etatOptions = [
    { name: 'Refusé', value: 'Refusé' },
    { name: 'Accepté', value: 'Accepté' }
  ];
  etatOptionsb = [
    { name: 'Black List', value: 'true' },
   
  ];
  constructor(
    private route: ActivatedRoute,
    private router : Router,
    private candidatureService: CandidatureService,private storageService: StorageService,    private service:UserService,
  ) {}
  showRCBoard = false;
  private roles: string[] = [];
  ngOnInit(): void {
    this.isLoggedIn = this.storageService.isLoggedIn();

    if (this.isLoggedIn) {
      const user = this.storageService.getUser();
      this.roles = user.roles;
      this.showRCBoard = this.roles.includes('ROLE_RC') || this.roles.includes('ROLE_RECRUTEUR');
    }
 
    const id = this.route.snapshot.paramMap.get('id');
    if (id !== null) {
      this.candidatureService.findCandidatureById(id).subscribe(
        (candidature) => {
          this.candidature = candidature;
        },
        (error) => {
          console.log(error);
        }
      );
    }

  }


  updatecandidateurs() {
    this.showSpinner = true;
    if (this.candidature) {

      this.candidatureService.updateAllCandidtaure(this.candidature).subscribe(
        (response) => {
          console.log(response);
          Swal.fire({
            position: 'top',
            icon: 'success',
            confirmButtonColor: '#25377A',
            title: " Votre modification est effectuer.",
            showConfirmButton: true,
          })     
             this.router.navigateByUrl('/offres');
           this.isSignUpFailed = false;
        },
        (error) => {
          console.log(error);
        }
      );
    }
    
  }

  candidats(id:string) {
    this.router.navigate(['candidats',id]);
  }
  
}