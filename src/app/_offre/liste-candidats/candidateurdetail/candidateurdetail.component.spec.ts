import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CandidateurdetailComponent } from './candidateurdetail.component';

describe('CandidateurdetailComponent', () => {
  let component: CandidateurdetailComponent;
  let fixture: ComponentFixture<CandidateurdetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CandidateurdetailComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CandidateurdetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
