import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CandidatureService } from 'src/app/_serviceCandidature/candidature.service';
import { StorageService } from 'src/app/_servicesUser/storage.service';
import { Utilisateur } from 'src/app/_servicesUser/utilisateur';

@Component({
  selector: 'app-liste-candidats',
  templateUrl: './liste-candidats.component.html',
  styleUrls: ['./liste-candidats.component.css']
})
export class ListeCandidatsComponent implements OnInit {
  utilisateurs: Utilisateur[] = [];
  utilisateur : Utilisateur= new Utilisateur();
  candidateurs: any;
  showRCBoard = false;
  isLoggedIn = false;
  filterMail: string = '';
  filterValide: boolean = false;
  filterEnAttente: boolean = false;
  filterAccepte: boolean = false;
  filterRefuse: boolean = false;
  etatCandidat: string = '';
  private roles: string[] = [];
  filterText: any;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private candidateurService: CandidatureService,
    private storageService: StorageService,
  ) {}

  ngOnInit(): void {
    this.isLoggedIn = this.storageService.isLoggedIn();

    if (this.isLoggedIn) {
      const user = this.storageService.getUser();
      this.roles = user.roles;
      this.showRCBoard = this.roles.includes('ROLE_RC') || this.roles.includes('ROLE_RECRUTEUR');
    }

    if (!this.showRCBoard) {
      this.router.navigate(['error']);
    }

    const offreId = this.route.snapshot.paramMap.get('id');

    if (offreId !== null) {
      this.candidateurService.getCandidateursByOffre(offreId).subscribe(
        (candidateurs) => {
          this.candidateurs = candidateurs;
        },
        (error) => {
          console.log(error);
        }
      );
    }
  }

  detail(candidateur: any) {
    this.router.navigate(['canddetail',candidateur.id]);
  }

  filterCandidats() {
    let filteredCandidates = [...this.candidateurs];

    const filterFunctions = [];

    if (this.filterAccepte) {
      filterFunctions.push((candidateur: any) => {
        return candidateur.etat.toLowerCase() === 'accepté';
      });
    }

    if (this.filterEnAttente) {
      filterFunctions.push((candidateur: any) => {
        return candidateur.etat.toLowerCase() === 'en attente';
      });
    }

    if (this.filterRefuse) {
      filterFunctions.push((candidateur: any) => {
        return candidateur.etat.toLowerCase() === 'refusé';
      });
    }

    if (filterFunctions.length > 0) {
      filteredCandidates = filterFunctions.reduce((prev, current) => {
        return prev.filter(current);
      }, filteredCandidates);
    }
    if (this.filterText) {
      filteredCandidates = filteredCandidates.filter((candidateur: any) =>
      candidateur.utilisateur.username.toLowerCase().includes(this.filterText.toLowerCase()) ||
      candidateur.utilisateur.email.toLowerCase().includes(this.filterText.toLowerCase())
      );
    }

    if (this.etatCandidat) {
      filteredCandidates = filteredCandidates.filter((candidateur: any) => {
        return candidateur.etat.toLowerCase() === this.etatCandidat.toLowerCase();
      });
    }




    if (this.filterMail) {
      filteredCandidates = filteredCandidates.filter((candidateur: any) =>
        candidateur.utilisateur.email.toLowerCase().includes(this.filterMail.toLowerCase())
      );
    }

    return filteredCandidates;
  }

  // method to check if only one checkbox is selected
  isOneCheckboxSelected() {
    return (
      (this.filterValide && !this.filterEnAttente && !this.filterRefuse) ||
      (!this.filterValide && this.filterEnAttente && !this.filterRefuse) ||
      (!this.filterValide && !this.filterEnAttente && this.filterRefuse)
    );
  }
}
