import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ServiceDemandeService } from 'src/app/_servicesDemande/-service-demande.service';
import { StorageService } from 'src/app/_servicesUser/storage.service';

@Component({
  selector: 'app-ddemande',
  templateUrl: './ddemande.component.html',
  styleUrls: ['./ddemande.component.css']
})
export class DdemandeComponent implements OnInit {
  isLoggedIn = false;
  showRCBoard = false;
  private roles: string[] = [];

  demande! : any;
  constructor(
    private router: Router,private route: ActivatedRoute,private storageService: StorageService,
    private demandeService: ServiceDemandeService
  ) { }

  ngOnInit(): void {
    this.isLoggedIn = this.storageService.isLoggedIn();

    if (this.isLoggedIn) {
      const user = this.storageService.getUser();
      this.roles = user.roles;
      this.showRCBoard = this.roles.includes('ROLE_RC') || this.roles.includes('ROLE_RECRUTEUR');
    }
    if (!this.showRCBoard){ this.router.navigate(['error']);
    }



    const id = this.route.snapshot.params['id'];
    this.demandeService.findDemandeById(id).subscribe(data => {
      this.demande = data;
           this.demande = data;
           this.demande = data.sort((a, b) => {
            return new Date(b.date_demande).getTime() - new Date(a.date_demande).getTime();
          });
          this.demande = data.sort((a, b) => {
            return new Date(b.date_integration).getTime() - new Date(a.date_integration).getTime();
          });
         });
  }
// retour() {
//     const id = this.route.snapshot.params['id'];
//     this.router.navigate(['addoffre', id]);
// }
navtoffre(id:string){
  
  this.router.navigate(['addoffre',id]);
 

}
}
