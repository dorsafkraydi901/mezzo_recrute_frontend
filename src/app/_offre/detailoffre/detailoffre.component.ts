import { HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Candidature } from 'src/app/_serviceCandidature/Candidature';
import { offre } from 'src/app/_servicesOffres/offre';
import { OffreService } from 'src/app/_servicesOffres/offre.service';
import { StorageService } from 'src/app/_servicesUser/storage.service';
import { UserService } from 'src/app/_servicesUser/user.service';
import { Utilisateur } from 'src/app/_servicesUser/utilisateur';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-detailoffre',
  templateUrl: './detailoffre.component.html',
  styleUrls: ['./detailoffre.component.css']
})
export class DetailoffreComponent implements OnInit {
  offree! : any;
  currentUser: any;
  utilisateur : Utilisateur= new Utilisateur();
  id ! : number ;
  private roles: string[] = [];
  isLoggedIn = false;
  showAdminBoard = false;
  showRCBoard = false;
  showModeratorBoard = false;
  showRecBoard = false;
  showRDBoard = false;
  showEmployeeBoard = false;
  showCandidatBoard = false;
  username?: string;
  utilisateurId: string | null = null;
  isSuccessful = false;
  isAddFailed = false;
  isSignUpFailed = false
  errorMessage = '';
  candidature: Candidature = new Candidature();
  offre!: offre;
  constructor(private route: ActivatedRoute,private routee : Router,private userservice : UserService,
    private offreService: OffreService ,  private storageService: StorageService , private service:UserService,) {  }

  ngOnInit(): void {
    this.isLoggedIn = this.storageService.isLoggedIn();
    if (this.isLoggedIn) {
      const user = this.storageService.getUser();
      this.roles = user.roles;
      this.showCandidatBoard = this.roles.includes('ROLE_CANDIDAT');
      this.userservice.findUserById(user.id.toString()).subscribe(data => {
        this.utilisateur = data;
      });
    }
  
   // Récupérer l'offre à modifier en fonction de l'ID
 const id = this.route.snapshot.params['id'];
 this.offreService.findOffreById(id).subscribe(data => {
   this.offre = data;
      });


      this.currentUser = this.storageService.getUser();

      this.isLoggedIn = this.storageService.isLoggedIn();
  
      if (this.isLoggedIn) {
        const user = this.storageService.getUser();
        this.roles = user.roles;
        this.showRCBoard = this.roles.includes('ROLE_RC') || this.roles.includes('ROLE_RECRUTEUR');
        this.username = user.username;
      }
    }
  
    candidats(id:string) {
      this.routee.navigate(['candidats',id]);
    }

// editoffre
updateOffre(id:string){
  this.routee.navigate(['modifier',id]);
 }

 //delete offre 
//  removeUser(id:string){
//   Swal.fire({
//     title: 'Confirmation de suppression',
//     text: "Tu est sur de supprimer cette offre?",
//     icon: 'warning',
//     showCancelButton: true,
//     confirmButtonColor: '#3085d6',
//     cancelButtonColor: '#d33',
//     confirmButtonText: 'Oui, supprimer!'
//   }).then((result) => {
//     if (result.isConfirmed) {
//       this.offreService.deleteOffre(id).subscribe(data => {
//         Swal.fire(
//           'Supprimé!',
//           'Offre supprimer avec succees.',
//           'success'
//         )
//         this.routee.navigate(['offres']);
//       });
//     }
//   })
  
 

// }

     
remove(id: string, updatedOffre: offre)  {
    
             
        
              // Mettre à jour l'offre avec les nouvelles informations
              this.offreService.updateAllOffreToHide(id, updatedOffre).subscribe(
                data => {
        
                  Swal.fire({
                    position: 'top',
                    icon: 'success',
                    text: "L'offre a été modifiée avec succès.",
                    showConfirmButton: false,
                    timer: 2000
                  })
                  this.routee.navigate(['offres']);
                },
                error => {
                  console.log(error);
                }
              );
             }    
         
        
        

navtopostuler(id:string){
  
  this.routee.navigate(['postuler',id]);
 

}

detail(id:string){
this.routee.navigate(['detail',id]);
}

  reloadPage(): void {
   window.location.reload();
  }
  myCandidature(id: string , offreId: string) {
 
      this.routee.navigate(['/cand',id, offreId , 'c']);
    
  }

  
  

 desactiverOffre(offreId: string) {
    // Récupère le token de la session de l'utilisateur

    this.offreService.disableOffre(offreId).subscribe({
      next: data => {
        console.log(data);
    
          this.routee.navigateByUrl('/offres');
    
      },
      error: err => {
        console.log(err);
      }
    });
  }

  activateOffre(offreId: string) {

    this.offreService.enableOffre(offreId).subscribe({
      next: data => {
        console.log(data);
    
          this.routee.navigateByUrl('/offres');
     
      },
      error: err => {
        console.log(err);
      }
    });
  }
  

}




