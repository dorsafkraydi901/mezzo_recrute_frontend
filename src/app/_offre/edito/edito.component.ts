import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { offre } from 'src/app/_servicesOffres/offre';
import { OffreService } from 'src/app/_servicesOffres/offre.service';
import { StorageService } from 'src/app/_servicesUser/storage.service';
import { PhotoserviceService } from 'src/app/photoservice/photoservice.service';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-edito',
  templateUrl: './edito.component.html',
  styleUrls: ['./edito.component.css']
})
export class EditoComponent implements OnInit {
  typeOffreOptions = ['Tous', 'Externe', 'Interne'];
  siteOptions = ['Tunis', 'Lille'];
  typecontrat = ['CDI', 'CDD' , 'Stage' , 'Alternance'];
  offre: offre = new offre();
  file: File | null = null;

  editform!: FormGroup;
  submitted = false;
  isLoggedIn = false;
  constructor( private offreService: OffreService,private storageService: StorageService,private photoService : PhotoserviceService,
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute) { }
    showRCBoard = false;
    private roles: string[] = [];
    showRecBoard = false;

  ngOnInit(): void {
    this.isLoggedIn = this.storageService.isLoggedIn();

    if (this.isLoggedIn) {
      const user = this.storageService.getUser();
      this.roles = user.roles;
      this.showRCBoard = this.roles.includes('ROLE_RC') || this.roles.includes('ROLE_RECRUTEUR');

    }
    if (!this.showRCBoard){ this.router.navigate(['error']);
    }


 // Récupérer l'offre à modifier en fonction de l'ID
 const id = this.route.snapshot.params['id'];
 this.offreService.findOffreById(id).subscribe(data => {
   this.offre = data;
   this.editform.patchValue(this.offre);
 });

 // Initialiser le formulaire de modification
 this.editform = this.formBuilder.group({
   titre: ['', Validators.required],
   lieu: ['', Validators.required],
   description: ['', Validators.required],
   type_poste: ['', Validators.required],
   type_contrat: ['', Validators.required],
   competence: ['', Validators.required],
   regime: ['', Validators.required],
   date_recrut: ['', Validators.required],
   typeOffre: ['', Validators.required],
   date_fin: new FormControl('', [Validators.required , dateFinValidator]),

 });
}
get titre(){
  return this.editform.get('titre'); 
}
get lieu(){
  return this.editform.get('lieu'); 
}
get description(){
  return this.editform.get('description'); 
}
get type_poste(){
  return this.editform.get('type_poste'); 
}
get type_contrat(){
  return this.editform.get('type_contrat'); 
}
get competence(){
  return this.editform.get('competence'); 
}
get regime(){
  return this.editform.get('regime'); 
}
get date_recrut(){
  return this.editform.get('date_recrut'); 
}
get date_fin(){
  return this.editform.get('date_fin'); 
}
get typeOffre(){
  return this.editform.get('typeOffre'); 
}

// Annuler la modification et retourner à la liste des offres
annuler() {
 this.router.navigate(['offres']);
}

// Soumettre la modification de l'offre
onSubmit() {
 this.submitted = true;

 // Stopper la soumission si le formulaire est invalide
 if (this.editform.invalid) {
   return;
 }

 // Mettre à jour l'offre avec les nouvelles informations
 this.offreService.updateAllOffre(this.offre).subscribe(
   data => {
    this.onSubmite();
     Swal.fire({
       position: 'top',
       icon: 'success',
       text: "L'offre a été modifiée avec succès.",
       showConfirmButton: false,
       timer: 2000
     })
     this.router.navigate(['offres']);
   },
   error => {
     console.log(error);
   }
 );
}
onFileSelected(event: any) {
  this.file = event.target.files[0];
}
onSubmite() {
  if (!this.file) {
    return;
  }

  const allowedTypes = ['image/jpeg', 'image/png', 'image/gif'];
  if (!allowedTypes.includes(this.file.type ?? '')) {
    console.error('Invalid file type. Only JPEG, PNG, and GIF images are allowed.');
    return;
  }

  // Rest of the code to upload the image
  const id = this.route.snapshot.params['id'];
  this.offreService.findOffreById(id).subscribe(data => {
    this.offre = data;
    this.editform.patchValue(this.offre);
  });

  const formData = new FormData();
  formData.append('image', this.file);

  this.photoService.uploadImageoffre(this.offre.id, formData).subscribe(
    () => {
      console.log('Image uploaded successfully');
    },
    error => {
      console.error('Error uploading image:', error);
    }
  );
}
isValidDateRange(): boolean {
  const dateDebut = new Date(this.offre.date_recrut);
  const dateFin = new Date(this.offre.date_fin);
  return dateDebut < dateFin;
}
}

function dateFinValidator(control: AbstractControl): ValidationErrors | null {
const date_fin = control.value;
const date_recrut = control.root.get('date_recrut')?.value;
if (date_recrut && date_fin && date_fin < date_recrut) {
return { dateFinBeforeDebut: true };
}
return null;
}
