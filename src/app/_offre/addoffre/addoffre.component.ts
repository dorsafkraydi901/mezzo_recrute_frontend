import { HttpHeaders } from '@angular/common/http';
import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { OffreService } from 'src/app/_servicesOffres/offre.service';
import {AbstractControl, FormControl, FormGroup, ValidationErrors, Validators} from '@angular/forms';
import Swal from 'sweetalert2';
import { StorageService } from 'src/app/_servicesUser/storage.service';
import { PhotoserviceService } from 'src/app/photoservice/photoservice.service';
import { offre } from 'src/app/_servicesOffres/offre';

@Component({
  selector: 'app-addoffre',
  templateUrl: './addoffre.component.html',
  styleUrls: ['./addoffre.component.css']
})
export class AddoffreComponent {
  showRCBoard = false;
  offre: offre = new offre();
  file: File | null = null;
  private roles: string[] = [];
  offreform = new FormGroup({
    date_fin: new FormControl('', [Validators.required , dateFinValidator]),
    titre: new FormControl('', [Validators.required]),
    //lieu: new FormControl('', [Validators.required]),
    //type_contrat: new FormControl(null, [Validators.required]),
    date_recrut: new FormControl('', [Validators.required]),
    //regime: new FormControl('', [Validators.required]),
    competence: new FormControl('', [Validators.required]),
    typeOffre: new FormControl('', [Validators.required]),
    desciption: new FormControl('', [Validators.required])
  });
  
  get date_fin(){
    return this.offreform.get('date_fin'); 
  }
  
  get titre(){
    return this.offreform.get('titre'); 
  }
  
  // get lieu(){
  //   return this.offreform.get('lieu'); 
  // }
  
   get typeOffre(){
    return this.offreform.get('typeOffre'); 
   }
  
  get date_recrut(){
    return this.offreform.get('date_recrut'); 
  }
  
  // get regime(){
  //   return this.offreform.get('regime'); 
  // }
  
  get competence(){
    return this.offreform.get('competence'); 
  }
  
  // get type_poste(){
  //   return this.offreform.get('type_poste'); 
  // }
  
  get desciption(){
    return this.offreform.get('desciption'); 
  }
  
  tags: string[] = [];
  form: any = {
    titre: null,
    lieu: null,
    description: null,
    type_poste: null,
    type_contrat: null,
    competence: null,
    regime: null,
    date_recrut: null,
    date_fin: null,
    typeOffre:null,
    Utilisateur: null,
  };
  currentStep = 1;
  isSuccessful = false;
  isAddFailed = false;
  isSignUpFailed = false
  errorMessage = '';
  isLoggedIn = false;
  type_contrat: any;
  type_poste: any;
  regime: any;
  lieu: any;

  constructor(
    private offreService: OffreService,
    private router: Router,private storageService: StorageService, private  photoService: PhotoserviceService,
    private route : ActivatedRoute
   
  ) {}

  ngOnInit(): void {
    this.isLoggedIn = this.storageService.isLoggedIn();

    if (this.isLoggedIn) {
      const user = this.storageService.getUser();
      this.roles = user.roles;
      this.showRCBoard = this.roles.includes('ROLE_RC') || this.roles.includes('ROLE_RECRUTEUR');
    }
    if (!this.showRCBoard){ this.router.navigate(['error']);
    }



  }

  onSubmit(): void {
 
    this.lieu = this.form.lieu.value; 
    this.type_poste = this.form.type_poste.value; 
    this.type_contrat = this.form.type_contrat.value; 
    this.regime = this.form.regime.value;
     
    // Récupère le token de la session de l'utilisateur
    const token = sessionStorage.getItem('auth-user');
    let tokenObj: any;
    if (token !== null) {
      tokenObj = JSON.parse(token);
    }
    const authToken = tokenObj.token;

    // Récupère les données du formulaire pour l'étape courante
    const {
      titre,
      lieu,
      description,
      type_poste,
      type_contrat,
      competence,
      regime,
      date_recrut,
      date_fin,
      typeOffre,
      utilisateur,
    } = this.form;

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + authToken,
      }),
    };

    // Envoie une requête pour ajouter l'offre
    this.offreService.addoffre(
        titre,
        lieu,
        description,
        type_poste,
        type_contrat,
        competence,
        regime,
        date_recrut,
        date_fin,
        typeOffre,
        utilisateur,
        httpOptions
      )
      .subscribe({
        next: data => {
          console.log(data);
        this.onSubmite();
        Swal.fire({
         position: 'top',
         icon: 'success',
         confirmButtonColor: '#25377A',
         title: "Félicitation! Votre ajout d'offre est réussie.",
         showConfirmButton: true,
       })     
          this.router.navigateByUrl('/offres');
        this.isSignUpFailed = false;
        },
        error: err => {
          this.errorMessage = "Échec de l'ajout";
          
          this.isSignUpFailed = true;
        }
      });
    }
    onSubmite() {
      if (!this.file) {
        return;
      }
    
      const allowedTypes = ['image/jpeg', 'image/png', 'image/gif'];
      if (!allowedTypes.includes(this.file.type ?? '')) {
        console.error('Invalid file type. Only JPEG, PNG, and GIF images are allowed.');
        return;
      }
    
      // Rest of the code to upload the image
      const id = this.route.snapshot.params['id'];
      this.offreService.findOffreById(id).subscribe(data => {
        this.offre = data;
        this.offreform.patchValue(this.offre);
      });
    
      const formData = new FormData();
      formData.append('image', this.file);
    
      this.photoService.uploadImageoffre(this.offre.id, formData).subscribe(
        () => {
          console.log('Image uploaded successfully');
        },
        error => {
          console.error('Error uploading image:', error);
        }
      );
    }
 
    onFileSelected(event: any) {
      this.file = event.target.files[0];
    }
    isValidDateRange(): boolean {
      const dateDebut = new Date(this.offre.date_recrut);
      const dateFin = new Date(this.offre.date_fin);
      return dateDebut < dateFin;
    }
}

function dateFinValidator(control: AbstractControl): ValidationErrors | null {
  const date_fin = control.value;
  const date_recrut = control.root.get('date_recrut')?.value;
  if (date_recrut && date_fin && date_fin < date_recrut) {
    return { dateFinBeforeDebut: true };
  }
  return null;
}
