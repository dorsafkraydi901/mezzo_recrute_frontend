

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { OffreService } from 'src/app/_servicesOffres/offre.service';
import { StorageService } from 'src/app/_servicesUser/storage.service';
import { UserService } from 'src/app/_servicesUser/user.service';
import { Utilisateur } from 'src/app/_servicesUser/utilisateur';
@Component({
  selector: 'app-show',
  templateUrl: './show.component.html',
  styleUrls: ['./show.component.css']
})
export class ShowComponent implements OnInit {
  offre! : any;
  searchTerm: string = '';
  filterText: string = '';
  offers: any[] = []; // Replace with your actual list of offers
  itemsPerPage = 5; // Number of items per page
  currentPage = 1; // Current page number
  filterValide: boolean = false;
  filterEnAttente: boolean = false;
  filterRefuse: boolean = false;
  etatDemande: string = '';
  etatDemandetype: string = '';
  etatDemandetypeoff: string = '';
  etatDemandecont: string = '';
  filterregime: boolean = false;
  filtertypecontrat: boolean = false;
  filtercompetence: boolean = false;
  showRCBoard = false;
  currentUser: any;
  private roles: string[] = [];
  isLoggedIn = false;
  showCandidatBoard= false;
  username?: string;
  utilisateur : Utilisateur= new Utilisateur();
  id ! : number ;
  isCandidat = false;
  constructor(private offreService: OffreService,private route : Router,  private storageService: StorageService ,  private service:UserService,) { }

  ngOnInit(): void {
    this.currentUser = this.storageService.getUser();
    this.isLoggedIn = this.storageService.isLoggedIn();
  
    if (this.isLoggedIn) {
      const user = this.storageService.getUser();
      this.roles = user.roles;
      this.showRCBoard = this.roles.includes('ROLE_RC') || this.roles.includes('ROLE_RECRUTEUR');
      this.showCandidatBoard = this.roles.includes('ROLE_CANDIDAT'); 
      this.username = user.username;
    }
    this.isLoggedIn = this.storageService.isLoggedIn();
    if (this.isLoggedIn) {
      const user = this.storageService.getUser();
      this.roles = user.roles;
    
      this.service.findUserById(user.id.toString()).subscribe(data => {
        this.utilisateur = data;
      });
    }

    if (!this.isLoggedIn || this.showCandidatBoard) {
      // User has the ROLE_CANDIDAT role or not logged in
      this.offreService.getOffresExterne().subscribe((data1) => {
        this.offreService.getOffresTous().subscribe((data2) => {
          const data = [...data1, ...data2];
          this.offre = data.sort((a, b) => {
            return new Date(b.date_publication).getTime() - new Date(a.date_publication).getTime();
          });
        });
      });
    } else {
      // User is logged in and wants to view internal offers
      this.offreService.getAlloffres().subscribe((data) => {
        this.offre = data.sort((a, b) => {
          return new Date(b.date_publication).getTime() - new Date(a.date_publication).getTime();
        });
      });
    }
    

  }




  detail(id:string){
  this.route.navigate(['detail',id]);
 }

 
 filterOffres(): any[] {
  let filterOffres = [...this.offre];

  const filterFunctions = [];

  if (this.filterValide) {
    filterFunctions.push((offre: any) => {
      return offre.lieu.toLowerCase() === 'Tunis';
    });
  }
  if (this.filterEnAttente) {
    filterFunctions.push((offre: any) => {
      return offre.lieu.toLowerCase() === 'Lille';
    });
  }
  if (this.filterregime) {
    filterFunctions.push((offre: any) => {
      return offre.regime.toLowerCase() === 'Temps Plein';
    });
  }
  if (this.filterregime) {
    filterFunctions.push((offre: any) => {
      return offre.regime.toLowerCase() === 'Temps Partiel';
    });
  }
  if (this.filtertypecontrat) {
    filterFunctions.push((offre: any) => {
      return offre.type_contrat.toLowerCase() === 'Stage';
    });
  }
  if (this.filtertypecontrat) {
    filterFunctions.push((offre: any) => {
      return offre.type_contrat.toLowerCase() === 'cdd';
    });
  }
  if (this.filtertypecontrat) {
    filterFunctions.push((offre: any) => {
      return offre.type_contrat.toLowerCase() === 'cdi';
    });
  }
  if (this.filtertypecontrat) {
    filterFunctions.push((offre: any) => {
      return offre.type_contrat.toLowerCase() === 'Alternance';
    });
  }
  if (this.filtercompetence) {
    filterFunctions.push((offre: any) => {
      return offre.typeOffre.toLowerCase() === 'Tous';
    });
  }
  if (this.filtercompetence) {
    filterFunctions.push((offre: any) => {
      return offre.typeOffre.toLowerCase() === 'Interne';
    });
  }
  if (this.filtercompetence) {
    filterFunctions.push((offre: any) => {
      return offre.typeOffre.toLowerCase() === 'Externe';
    });
  }
  
  if (this.etatDemande) {
    filterOffres = filterOffres.filter((offre: any) =>
      offre.lieu.toLowerCase() === this.etatDemande.toLowerCase() 
      ||
      offre.regime.toLowerCase() === this.etatDemande.toLowerCase()
      ||
      offre.type_contrat.toLowerCase() === this.etatDemande.toLowerCase()
      ||
      offre.typeOffre.toLowerCase() === this.etatDemande.toLowerCase()
    );
  }
  if (this.etatDemandetype) {
    filterOffres = filterOffres.filter((offre: any) =>
      offre.regime.toLowerCase() === this.etatDemandetype.toLowerCase()
   
    );
  }
  if (this.etatDemandecont) {
    filterOffres = filterOffres.filter((offre: any) =>
    offre.type_contrat.toLowerCase() === this.etatDemandecont.toLowerCase()
   
    );
  }
  if (this.etatDemandetypeoff) {
    filterOffres = filterOffres.filter((offre: any) =>
    offre.typeOffre.toLowerCase() === this.etatDemandetypeoff.toLowerCase()
   
    );
  }

  if (this.filterText) {
    filterOffres = filterOffres.filter((offre: any) =>
    offre.titre.toLowerCase().includes(this.filterText.toLowerCase())
     || 
   offre.lieu.toLowerCase().includes(this.filterText.toLowerCase())
    || 
   offre.regime.toLowerCase().includes(this.filterText.toLowerCase())
   || 
  offre.typeOffre.toLowerCase().includes(this.filterText.toLowerCase())
    );
  }
  const startIndex = (this.currentPage - 1) * this.itemsPerPage;
  const endIndex = startIndex + this.itemsPerPage;
  filterOffres = filterOffres.slice(startIndex, endIndex);
  return filterOffres;
}

mescandidateurs(id: string): void {
  this.route.navigate(['/mes-candidatures', id]);
}
get totalPages(): number {
  return Math.ceil(this.offers.length / this.itemsPerPage);
}

get pages(): number[] {
  return Array.from({ length: this.totalPages }, (_, index) => index + 1);
}

goToPage(pageNumber: number) {
  this.currentPage = pageNumber;
}

}
