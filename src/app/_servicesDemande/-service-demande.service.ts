import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Demande, Validation } from './demande';
import { Observable } from 'rxjs';
import { Utilisateur } from '../_servicesUser/utilisateur';
const API_URL = 'http://localhost:8080/api/test/demandes/';
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
@Injectable({
  providedIn: 'root'
})
export class ServiceDemandeService {
  updateAllOffre(demande: any) {
    throw new Error('Method not implemented.');
  }

  constructor(private http: HttpClient) { }

    // get methode for all demandes

  getAllDemmandes() {
    return this.http.get<Demande[]>(API_URL+"getall");
  }
      // methode get un demande avec his id

  findDemandeById(id: string) {
    return this.http.get<Demande>(API_URL + "getdemande/" + id);
  }
//update function for the admin when he gonna update the validation
  updateDeamandes(o: Demande) {
    return this.http.put<Demande>(API_URL + o.id, o);
} 
  
// this update only for the RD

updatedemande(o: Demande) {
  return this.http.put<Demande>(API_URL+ "Onedemande/" + o.id, o);

} 
  //    delete methode  for deleting un demande
deletedemande(id : string){
  return this.http.delete<Demande[]>(API_URL+id);
}
getDemandesForUtilisateur(): Observable<Demande[]> {
  const token = sessionStorage.getItem('auth-user');
  let tokenObj : any; 
  if (token !== null) {
    tokenObj =  JSON.parse(token);
  }
  const authToken = tokenObj.token;
  const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + authToken
    })
  };
  return this.http.get<Demande[]>(API_URL + 'demandesutil', httpOptions);
}

createDemande(site: string, date_demande: Date, date_integration: Date, type_contrat: string, nbr_collab: number,
  temps_travail: string, profile_chercher: string, 
  poste_recherche: string,expiration: Date , description : string,
   validation: Validation , utilisateur : Utilisateur ,  httpOptions: any 
): Observable<any> {

 return this.http.post(
  API_URL + 'adddemande',
   {
    site,
    date_demande,
    date_integration,
    type_contrat,
    nbr_collab,
    temps_travail,
    profile_chercher,
    poste_recherche,
    expiration,
    description,
    validation, 
    utilisateur ,

   },
   httpOptions
 );
}
getValideDemandes(): Observable<Demande[]> {
  return this.http.get<Demande[]>(API_URL+"valide");
 
}

}