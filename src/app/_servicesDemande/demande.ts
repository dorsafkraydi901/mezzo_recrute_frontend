import { offre } from "../_servicesOffres/offre";
import { Utilisateur } from "../_servicesUser/utilisateur";

export enum Validation {
    EN_ATTENTE = "EN_ATTENTE",
    REFUSE = "REFUSE",
    VALIDE = "VALIDE"
  }
 
export class Demande {
    sort(arg0: (a: any, b: any) => number): any {
      throw new Error('Method not implemented.');
    }
    id!: string;
    site!: string;
    date_demande!: Date;
    date_integration!: Date;
    type_contrat!: string;
    nbr_collab!: number;
    temps_travail!: string;
    profile_chercher!: string;
    poste_recherche!: string;
    expiration!: Date;
    description!: string;
    validation!: Validation;
    utilisateur!: Utilisateur;
    remarque !: string;
    offres!: offre;
  }