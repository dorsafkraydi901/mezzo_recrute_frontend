import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Validators } from '@angular/forms';
@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {
  contactForm!: FormGroup;
  showSpinner: boolean = true;
  constructor() { }

  ngOnInit(): void {
    this.contactForm = new FormGroup({
      name: new FormControl('', Validators.required),
      email:new FormControl('',[Validators.required , Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$") ]),
      message: new FormControl('', Validators.required)
    });
  }

  onSubmit() {
    const formData = this.contactForm.value;
    const message = `Nom : ${formData.name}\nE-mail : ${formData.email}\nMessage : ${formData.message}`;

    // Envoyer le message à l'adresse e-mail de l'entreprise
    this.sendEmail('dorsafkraydi@gmail.com', 'Nouveau message ', message);
  }

  sendEmail(to: string, subject: string, body: string) {
    const emailSubject = encodeURIComponent(subject);
    const emailBody = encodeURIComponent(body);
    const mailtoLink = `mailto:${to}?subject=${emailSubject}&body=${emailBody}`;

    window.open(mailtoLink, '_blank');
  }
}
