import { Component } from '@angular/core';
import { NavigationStart, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AuthService } from './_servicesUser/auth.service';
import { StorageService } from './_servicesUser/storage.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title(title: any) {
    throw new Error('Method not implemented.');
  }
  showMenu:boolean = false;

  private roles: string[] = [];
  isLoggedIn = false;
  showAdminBoard = false;
  showRCBoard = false;
  showModeratorBoard = false;
  showRecBoard = false;
  showRDBoard = false;
  showEmployeeBoard = false;
  username?: string;
 
  eventBusSub?: Subscription;

  constructor(
    private storageService: StorageService,
    private authService: AuthService,
    private router: Router
  ) {

    router.events.forEach((event) => {
      if (event instanceof NavigationStart) {
        this.showMenu = event.url !== "/registre" && event.url !== "/login"
      }
    })
  }
  ngOnInit(): void {
    
    this.isLoggedIn = this.storageService.isLoggedIn();

    if (this.isLoggedIn) {
      const user = this.storageService.getUser();
      this.roles = user.roles;

      this.showAdminBoard = this.roles.includes('ROLE_ADMIN');
      this.showRCBoard = this.roles.includes('ROLE_RC');
      this.showRecBoard = this.roles.includes('ROLE_RECRUTEUR');
      this.showRDBoard = this.roles.includes('ROLE_RD');

      this.showEmployeeBoard = this.roles.includes('ROLE_EMPLOYEE');

      this.username = user.username;
    }
  }


}
