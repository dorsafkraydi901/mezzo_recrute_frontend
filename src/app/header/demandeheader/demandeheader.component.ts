import { Component, OnInit } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/_servicesUser/auth.service';
import { StorageService } from 'src/app/_servicesUser/storage.service';
import { EventBusService } from 'src/app/_shared/event-bus.service';

@Component({
  selector: 'app-demandeheader',
  templateUrl: './demandeheader.component.html',
  styleUrls: ['./demandeheader.component.css']
})
export class DemandeheaderComponent implements OnInit {

  showMenu:boolean = false;
  currentUser: any;
  private roles: string[] = [];
  isLoggedIn = false;
  showAdminBoard = false;
  showRCBoard = false;
  showModeratorBoard = false;
  showRecBoard = false;
  showRDBoard = false;
  showEmployeeBoard = false;
  showCandidatBoard = false;
  username?: string;
 
  eventBusSub?: Subscription;
  constructor(
    private storageService: StorageService,
    private authService: AuthService,
    private eventBusService: EventBusService,
    private router: Router
  ){

    router.events.forEach((event) => {
      if (event instanceof NavigationStart) {
        this.showMenu = event.url !== "/login" && event.url !== "/register"
      }
    })
  }

  ngOnInit(): void {
    this.currentUser = this.storageService.getUser();

    this.isLoggedIn = this.storageService.isLoggedIn();

    if (this.isLoggedIn) {
      const user = this.storageService.getUser();
      this.roles = user.roles;

      this.showAdminBoard = this.roles.includes('ROLE_ADMIN');
      this.showRCBoard = this.roles.includes('ROLE_RC');
      this.showRecBoard = this.roles.includes('ROLE_RECRUTEUR');
      this.showRDBoard = this.roles.includes('ROLE_RD');

      this.showEmployeeBoard = this.roles.includes('ROLE_EMPLOYEE');
      this.showCandidatBoard = this.roles.includes('ROLE_CANDIDAT');

      this.username = user.username;
    }
   
    this.eventBusSub = this.eventBusService.on('logout', () => {
      this.logout();
    });
  }
  

  logout(): void {
    this.router.navigate(['acceuil']);
    this.storageService.signOut();
  
  }

}
