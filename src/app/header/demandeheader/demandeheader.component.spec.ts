import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DemandeheaderComponent } from './demandeheader.component';

describe('DemandeheaderComponent', () => {
  let component: DemandeheaderComponent;
  let fixture: ComponentFixture<DemandeheaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DemandeheaderComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DemandeheaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
