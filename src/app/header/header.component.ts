import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationStart, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AuthService } from '../_servicesUser/auth.service';
import { StorageService } from '../_servicesUser/storage.service';
import { EventBusService } from '../_shared/event-bus.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent {

  showMenu:boolean = false;
  currentUser: any;
  private roles: string[] = [];
  isLoggedIn = false;
  showAdminBoard = false;
  showRCBoard = false;
  showModeratorBoard = false;
  showRecBoard = false;
  showRDBoard = false;
  showEmployeeBoard = false;
  showCandidatBoard = false;
  username?: string;
  currentPath: string | undefined;

  eventBusSub?: Subscription;
  constructor(
    private storageService: StorageService,
    private authService: AuthService,private route: ActivatedRoute,
    private eventBusService: EventBusService,
    private router: Router
  ){

    router.events.forEach((event) => {
      if (event instanceof NavigationStart) {
        this.showMenu = event.url !== "/login" && event.url !== "/register"
      }
    })
  }

  ngOnInit(): void {
    this.route.url.subscribe(url => {
    this.currentPath = url[0].path;
  });
    this.currentUser = this.storageService.getUser();

    this.isLoggedIn = this.storageService.isLoggedIn();

    if (this.isLoggedIn) {
      const user = this.storageService.getUser();
      this.roles = user.roles;

      this.showAdminBoard = this.roles.includes('ROLE_ADMIN');
      this.showRCBoard = this.roles.includes('ROLE_RC');
      this.showRecBoard = this.roles.includes('ROLE_RECRUTEUR');
      this.showRDBoard = this.roles.includes('ROLE_RD');

      this.showEmployeeBoard = this.roles.includes('ROLE_EMPLOYEE');
      this.showCandidatBoard = this.roles.includes('ROLE_CANDIDAT');

      this.username = user.username;
    }
   
    this.eventBusSub = this.eventBusService.on('logout', () => {
      this.logout();
    });
  }
  

  logout(): void {
    this.authService.logout();
    this.router.navigate(['/login']);
  }
}

