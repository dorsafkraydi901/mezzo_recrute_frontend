import { Component } from '@angular/core';
import { NavigationStart, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AuthService } from '../../_servicesUser/auth.service';
import { StorageService } from '../../_servicesUser/storage.service';
import { EventBusService } from '../../_shared/event-bus.service';
import { NotificationService } from 'src/app/_servicenotification/notification.service';
import { Notification } from 'src/app/_servicenotification/Notification';

@Component({
  selector: 'app-header-profil',
  templateUrl: './header-profil.component.html',
  styleUrls: ['./header-profil.component.css']
})
export class HeaderProfilComponent{

  showMenu:boolean = false;
  currentUser: any;
  private roles: string[] = [];
  isLoggedIn = false;
  showAdminBoard = false;
  showRCBoard = false;
  showRCRBoard = false;
  showModeratorBoard = false;
  showRecBoard = false;
  showRDBoard = false;
  showEmployeeBoard = false;
  showCandidatBoard = false;
  username?: string;
  notificationCount!: number ;
  notificationCountvalide!: number ;
  eventBusSub?: Subscription;
  notifications: Notification[] = [];
unreadCount: any;
dropdownOpen = false;
showNotificationPopup = false;

  constructor(
    private storageService: StorageService,private notificationService : NotificationService,
    private authService: AuthService,
    private eventBusService: EventBusService,
    private router: Router
  ){

    router.events.forEach((event) => {
      if (event instanceof NavigationStart) {
        this.showMenu = event.url !== "/login" && event.url !== "/register"
      }
    })
  }

  ngOnInit(): void {
    this.updateNotificationCount();
    this. updateNotificationCountvalide();
    this.currentUser = this.storageService.getUser();

    this.isLoggedIn = this.storageService.isLoggedIn();

    if (this.isLoggedIn) {
      const user = this.storageService.getUser();
      this.roles = user.roles;

      this.showAdminBoard = this.roles.includes('ROLE_ADMIN');
      this.showRCBoard = this.roles.includes('ROLE_RC');
      this.showRecBoard = this.roles.includes('ROLE_RECRUTEUR');
      this.showRDBoard = this.roles.includes('ROLE_RD');
      this.showRCRBoard = this.roles.includes('ROLE_RC') || this.roles.includes('ROLE_RECRUTEUR');

      this.showEmployeeBoard = this.roles.includes('ROLE_EMPLOYEE');
      this.showCandidatBoard = this.roles.includes('ROLE_CANDIDAT');

      this.username = user.username;
    }
   

    this.eventBusSub = this.eventBusService.on('logout', () => {
      this.logout();
    });
  }
  


  logout(): void {
    if (this.router.url === '/acceuil') {
      window.location.reload();
    } else {
      this.router.navigate(['acceuil']);
    }
    this.storageService.signOut();
  }
  updateNotificationCount() {
    this.notificationService.getAddNotificationsCount().subscribe(
      (count: number) => {
        this.notificationCount = count;
      },
      (error: any) => {
        // Handle error if needed
        console.log(error);
      }
    );
  }
  showNotifications() {
    this.notificationService.getNotifications().subscribe((response: any) => {
      this.notifications = response.notifications;
      this.notificationCount = this.notifications.length; // Update the notificationCount
      // Handle the logic to display the notifications (e.g., show in a dropdown, open a modal, etc.)
      console.log(this.notifications); // Example: Log the notifications to the console
    
      // Reset the notification count to 0
      this.notificationCount = 0;
    });
  }




  showNotificationsvalide() {
    this.notificationService.getNotificationsvalide().subscribe((response: any) => {
      this.notifications = response.notifications;
      this.notificationCount = this.notifications.length; // Update the notificationCount
      // Handle the logic to display the notifications (e.g., show in a dropdown, open a modal, etc.)
      console.log(this.notifications); // Example: Log the notifications to the console
    
      // Reset the notification count to 0
      this.notificationCountvalide = 0;
    });
  }
    updateNotificationCountvalide() {
    this.notificationService.getAddNotificationsCountvalide().subscribe(
      (count: number) => {
        this.notificationCountvalide = count;
      },
      (error: any) => {
        // Handle error if needed
        console.log(error);
      }
    );
  }











  
  toggleDropdown(): void {
    this.dropdownOpen = !this.dropdownOpen;
  }
  resetNotificationCount(): void {
    this.notificationCount = 0;
  }
  resetNotificationCountvalide(): void {
    this.notificationCountvalide = 0;
  }
  openNotificationPopup(): void {
    this.showNotificationPopup = true;
  }
  
  closeNotificationPopup(): void {
    this.showNotificationPopup = false;
  }
}












