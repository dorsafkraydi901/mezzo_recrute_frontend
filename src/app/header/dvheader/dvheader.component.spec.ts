import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DvheaderComponent } from './dvheader.component';

describe('DvheaderComponent', () => {
  let component: DvheaderComponent;
  let fixture: ComponentFixture<DvheaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DvheaderComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DvheaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
