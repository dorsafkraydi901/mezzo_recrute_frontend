import { HttpClient, HttpEvent, HttpEventType, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, catchError, map, throwError } from 'rxjs';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
@Injectable({
  providedIn: 'root'
})
export class FileService {
  private apiEndpoint = 'http://localhost:8080/api';

  constructor(private http: HttpClient) { }
  
  uploadFile(file: File, token: string): Observable<any> {
    const formData = new FormData();
    formData.append('imageFile', file);

    const headers = new HttpHeaders({
      Authorization: `Bearer ${token}`
    });

    return this.http.post<any>(`${this.apiEndpoint}/upload`, formData, {
      headers,
      reportProgress: true,
      observe: 'events'
    }).pipe(
      map(event => {
        if (event.type === HttpEventType.UploadProgress) {
          const progress = event.total ? Math.round(100 * event.loaded / event.total) : 0;
          return { progress };
        } else if (event.type === HttpEventType.Response) {
          const contentType = event.headers.get('Content-Type');
          if (contentType?.startsWith('image/')) {
            const blob = new Blob([event.body], { type: contentType });
            return URL.createObjectURL(blob);
          } else if (contentType === 'application/pdf') {
            const blob = new Blob([event.body], { type: 'application/pdf' });
            return URL.createObjectURL(blob);
          } else {
            return event.body;
          }
        }
      }),
      catchError(error => {
        console.error(error);
        return throwError('Une erreur est survenue lors de l\'envoi du fichier.');
      })
    );
  }

  getImage(id: string): Observable<any> {
    return this.http.get(`${this.apiEndpoint}/test/file/download/${id}`, {
      responseType: 'arraybuffer'
    });
  }



}
