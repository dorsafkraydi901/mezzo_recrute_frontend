export class File {
  id: string ='';
    fileType: string = '';
    fileSize: string = '';
    file: ArrayBuffer | string | null = null;
  }