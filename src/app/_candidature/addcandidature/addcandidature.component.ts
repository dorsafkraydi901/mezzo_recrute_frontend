import { HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Candidature } from 'src/app/_serviceCandidature/Candidature';
import { CandidatureService } from 'src/app/_serviceCandidature/candidature.service';
import { offre } from 'src/app/_servicesOffres/offre';
import { OffreService } from 'src/app/_servicesOffres/offre.service';
import { StorageService } from 'src/app/_servicesUser/storage.service';
import { Utilisateur } from 'src/app/_servicesUser/utilisateur';
import { ToastrService } from 'ngx-toastr';
import Swal from 'sweetalert2';
import { AbstractControl, ValidatorFn } from '@angular/forms';
import { PhotoserviceService } from 'src/app/photoservice/photoservice.service';
import { UserService } from 'src/app/_servicesUser/user.service';

@Component({
  selector: 'app-addcandidature',
  templateUrl: './addcandidature.component.html',
  styleUrls: ['./addcandidature.component.css']
})

export class AddcandidatureComponent {
  showSpinner: boolean = false;
  files: File | null = null;
  candidatureForm = new FormGroup({
    description: new FormControl('', [Validators.required]),
    titre: new FormControl('', [Validators.required]),
    etablissement: new FormControl('', [Validators.required]),
    annee: new FormControl('', [Validators.required]),
    dateFin: new FormControl('', [Validators.required, dateFinValidator]),
    entreprise: new FormControl('', [Validators.required]),
    poste: new FormControl('', [Validators.required]),
    dateDebut: new FormControl('', [Validators.required])
  });
  
  isValidDateRange(): boolean {
    const dateDebut = new Date(this.candidature.experience.dateDebut);
    const dateFin = new Date(this.candidature.experience.dateFin);
    return dateDebut < dateFin;
  }
  get titre(){
    return this.candidatureForm.get('titre'); 
  }
  get etablissement(){
    return this.candidatureForm.get('etablissement'); 
  }
  get annee(){
    return this.candidatureForm.get('annee'); 
  }
  get dateFin(){
    return this.candidatureForm.get('dateFin'); 
  }
  get entreprise(){
    return this.candidatureForm.get('entreprise'); 
  }
  get poste(){
    return this.candidatureForm.get('poste'); 
  }

  get dateDebut(){
    return this.candidatureForm.get('dateDebut'); 
  }
 
  currentUser : Utilisateur=this.storageService.getUser();
  utilisateur : Utilisateur=this.storageService.getUser();
  isLoggedIn = false;
  candidature: Candidature = {
    date_depot: new Date(),
    etat: 'en_attente',
    blocklist :'',
    remarque: '',
    canaux: '',
    utilisateur: new Utilisateur(),
    diplome: {
      titre: '',
      etablissement: '',
      annee: ''
    },
    experience: {
      entreprise: '',
      poste: '',
      dateDebut: '',
      dateFin: '',
      description: ''
    },
    offreId: '',
    id: '',
    offres: new offre
  };
  offre!: offre;
  offreId!: string;
  isSuccessful = false;
  isAddFailed = false;
  isSignUpFailed = false
  errorMessage = '';
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private candidatureService: CandidatureService,
    private storageService: StorageService,
    private offreService: OffreService,  private service:UserService,
    private toastr: ToastrService,private  photoService: PhotoserviceService
  ) {  this.currentUser = new Utilisateur();}

  ngOnInit(): void {
    this.isLoggedIn = this.storageService.isLoggedIn();
    this.currentUser = this.storageService.getUser();

    // Récupérer l'offre à modifier en fonction de l'ID
    const id = this.route.snapshot.params['id'];
    this.offreService.findOffreById(id).subscribe(data => {
      this.offre = data;
      this.candidature.offreId = data.id;
    });
  }

  onSubmit() {
    this.showSpinner = true;
    const token = sessionStorage.getItem('auth-user');
    let tokenObj: any;
    if (token !== null) {
      tokenObj = JSON.parse(token);
    }

    const authToken = tokenObj.token;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + authToken
      })
    };

   
    this.candidature.offres = this.offre;
    this.candidatureService.createCandidature(this.candidature, httpOptions)
    .subscribe({
      next: data => {
        this.cvupdate();
        console.log(data);

      Swal.fire({
       position: 'top',
       icon: 'success',
       confirmButtonColor: '#25377A',
       title: "Félicitation! votre candidature a étè bien reçu, bon courage",
       showConfirmButton: true,
     })     
      this.router.navigateByUrl('/offres');
      this.isSignUpFailed = false;
      },
      error: err => {
        this.errorMessage = "Échec de l'ajout";
        this.isSignUpFailed = true;
        if (err.status === 401 && err.error.message === "vous avez deja postuler sur cette offre") {
          this.toastr.error("Vous avez déjà postulé pour cette offre.");
        }


      }


    });
  }
  onCvSelected(event: any) {
    this.files = event.target.files[0];
  }
  cvupdate(){
    if (!this.files) {
      return;
    }
    const user = this.storageService.getUser();
      this.service.findUserById(user.id.toString()).subscribe(data => {
        this.utilisateur = data;
       });
    const formData = new FormData();
    formData.append('file', this.files);
    this.photoService.uploadCv(user.id, formData).subscribe(
      () => {
        console.log('cv uploaded successfully');
      },
      error => {
        console.error('Error uploading cv:', error);
      }
    );
  }
  
}

function dateFinValidator(control: AbstractControl): ValidationErrors | null {
  const dateFin = control.value;
  const dateDebut = control.root.get('dateDebut')?.value;
  if (dateDebut && dateFin && dateFin < dateDebut) {
    return { dateFinBeforeDebut: true };
  }
  return null;
}
