import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddcandidatureComponent } from './addcandidature.component';

describe('AddcandidatureComponent', () => {
  let component: AddcandidatureComponent;
  let fixture: ComponentFixture<AddcandidatureComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddcandidatureComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AddcandidatureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
