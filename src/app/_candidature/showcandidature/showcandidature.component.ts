import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Candidature } from 'src/app/_serviceCandidature/Candidature';
import { CandidatureService } from 'src/app/_serviceCandidature/candidature.service';
import { StorageService } from 'src/app/_servicesUser/storage.service';

@Component({
  selector: 'app-showcandidature',
  templateUrl: './showcandidature.component.html',
  styleUrls: ['./showcandidature.component.css']
})
export class ShowcandidatureComponent implements OnInit {
  candidatures: Candidature[] = [];
  utilisateurId?: string;
  isLoggedIn = false;
  filterText: any;
  etatCandidat: string = '';

  constructor(
    private candidatureService: CandidatureService,private storageService: StorageService,
    private route: ActivatedRoute,
    private router : Router,
  ) { }

  ngOnInit(): void {
  
    this.isLoggedIn = this.storageService.isLoggedIn();
  
    if (this.isLoggedIn) {
      const user = this.storageService.getUser();
 
    }
    this.route.params.subscribe(params => {
      this.utilisateurId = params['id'];
      this.candidatureService.getCandidaturesForUtilisateur(this.utilisateurId!)
        .subscribe(candidatures => this.candidatures = candidatures);
    });
  }
  
  detail(id: string) {
    this.router.navigate(['canddetail',id]);
  }
  filterCandidats() {
   
  }
}