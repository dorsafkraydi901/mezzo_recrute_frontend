import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowcandidatureComponent } from './showcandidature.component';

describe('ShowcandidatureComponent', () => {
  let component: ShowcandidatureComponent;
  let fixture: ComponentFixture<ShowcandidatureComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShowcandidatureComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ShowcandidatureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
