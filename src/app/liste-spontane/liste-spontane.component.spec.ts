import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListeSpontaneComponent } from './liste-spontane.component';

describe('ListeSpontaneComponent', () => {
  let component: ListeSpontaneComponent;
  let fixture: ComponentFixture<ListeSpontaneComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListeSpontaneComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ListeSpontaneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
