import { Component, OnInit } from '@angular/core';
import { Utilisateur } from '../_servicesUser/utilisateur';
import { UserService } from '../_servicesUser/user.service';
import { StorageService } from '../_servicesUser/storage.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-liste-spontane',
  templateUrl: './liste-spontane.component.html',
  styleUrls: ['./liste-spontane.component.css']
})
export class ListeSpontaneComponent implements OnInit {
  users: Utilisateur[] = [];
  showRCBoard = false;
  private roles: string[] = [];
  showRecBoard = false;
  isLoggedIn = false;
  constructor(   private userService:UserService ,private storageService: StorageService,    private router: Router,) { }
  ngOnInit(): void {
    this.isLoggedIn = this.storageService.isLoggedIn();

    if (this.isLoggedIn) {
      const user = this.storageService.getUser();
      this.roles = user.roles;
      this.showRCBoard = this.roles.includes('ROLE_RC') || this.roles.includes('ROLE_RECRUTEUR');

    }
    if (!this.showRCBoard){ this.router.navigate(['error']);
    }

    this.userService.getAllusers().subscribe(
      (data: Utilisateur[]) => {
        this.users = data;
      },
      (error) => {
        console.error(error);
      }
    );
  }
}  