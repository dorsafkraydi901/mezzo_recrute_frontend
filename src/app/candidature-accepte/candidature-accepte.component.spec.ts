import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CandidatureAccepteComponent } from './candidature-accepte.component';

describe('CandidatureAccepteComponent', () => {
  let component: CandidatureAccepteComponent;
  let fixture: ComponentFixture<CandidatureAccepteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CandidatureAccepteComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CandidatureAccepteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
