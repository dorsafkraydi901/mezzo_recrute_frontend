import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Candidature } from 'src/app/_serviceCandidature/Candidature';
import { CandidatureService } from 'src/app/_serviceCandidature/candidature.service';
import { StorageService } from 'src/app/_servicesUser/storage.service';

@Component({
  selector: 'app-candidature-accepte',
  templateUrl: './candidature-accepte.component.html',
  styleUrls: ['./candidature-accepte.component.css']
})
export class CandidatureAccepteComponent implements OnInit {
  candidatures: Candidature[] | undefined;
  isLoggedIn = false;
  showRCBoard = false;
  private roles: string[] = [];
  showRDBoard = false;
  etatOptions = [
    { name: 'Refusé', value: 'Refusé' },
    { name: 'Accepté', value: 'Accepté' }
  ];
  constructor(private storageService: StorageService,     private router: Router, private candidatureService: CandidatureService) { }

  ngOnInit(): void {
//testy si l utilisateur est de role rc et rect ou non
this.isLoggedIn = this.storageService.isLoggedIn();

if (this.isLoggedIn) {
  const user = this.storageService.getUser();
  this.roles = user.roles;
  this.showRCBoard = this.roles.includes('ROLE_RC') || this.roles.includes('ROLE_RECRUTEUR');

}
if (!this.showRCBoard){ this.router.navigate(['error']);
}
this.getAllCandidaturesAcceptees();
}

getAllCandidaturesAcceptees(): void {
  this.candidatureService.getAllCandidaturesAcceptees()
  
    .subscribe(
      candidatures => this.candidatures = candidatures);
}
detail(id: string) {
  this.router.navigate(['canddetail',id]);
}
}
