import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { PasswordResetServiceService } from '../_servicesUser/password-reset-service.service';
import Swal from 'sweetalert2';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {
  
  resetPasswordForm!: FormGroup;

  newPassword: string = ''; // Utilisez une variable distincte pour newPassword
  confirmPassword: string = ''; // Utilisez une variable distincte pour confirmPassword

  isResetFailed = false;
  errorMessage = '';
  email: string = '';
  token: string = '';
  constructor(
    private passwordResetServiceService: PasswordResetServiceService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
        this.email = params['email'];
        this.token = params['token'];
    });
    
    this.resetPasswordForm = new FormGroup({
        'email': new FormControl(this.email), // Ajoutez un champ email avec la valeur récupérée
        'newPassword': new FormControl('', [Validators.required, Validators.minLength(6)]),
        'confirmPassword': new FormControl('', [Validators.required, Validators.minLength(6)])
    });
}

  onSubmit(): void {
    if (this.newPassword !== this.confirmPassword) {
      this.errorMessage = 'Les mots de passe ne correspondent pas.';
      this.isResetFailed = true;
      return;
    }

    // Vous devriez utiliser la valeur du champ email du formulaire
    const email = this.resetPasswordForm.get('email')?.value;

    // Appelez le service pour réinitialiser le mot de passe
    this.passwordResetServiceService.resetPassword(this.email, this.newPassword).subscribe(
      () => {
        // Réinitialisation réussie, affichez le message SweetAlert2
        Swal.fire({
          position: 'top',
          icon: 'success',
          text: "Votre mot de passe a été réinitialisé avec succès. Veuillez vous reconnecter.",
          showConfirmButton: false,
          timer: 3600
        }).then(() => {
          // Redirigez l'utilisateur vers la page de connexion après le message SweetAlert2
          this.router.navigateByUrl('/login');
        });
      },
      (error) => {
        console.error('Erreur lors de la réinitialisation du mot de passe :', error);
        this.errorMessage = 'Erreur lors de la réinitialisation du mot de passe.';
        this.isResetFailed = false;
      }
    );
  }
}
