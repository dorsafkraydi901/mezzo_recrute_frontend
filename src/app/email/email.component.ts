import { Component } from '@angular/core';
import { PasswordResetServiceService } from '../_servicesUser/password-reset-service.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

@Component({
  selector: 'app-email',
  templateUrl: './email.component.html',
  styleUrls: ['./email.component.css']
})
export class EmailComponent {

  isLoggedIn = false;
  isLoginFailed = false;

  
  email: string = '';
  form: any = {
    email: null,

  };
  constructor(
    private passwordResetServiceService: PasswordResetServiceService,
    private router: Router
  ) {}


  
  requestPasswordReset() {
    this.passwordResetServiceService.requestPasswordReset(this.email).subscribe(
      () => {
        Swal.fire({
          position: 'top',
          icon: 'success',
          text: "Un e-mail de réinitialisation de mot de passe a été envoyé à votre adresse e-mail.",
          showConfirmButton: false,
          timer: 2000
        }).then(() => {
          this.router.navigateByUrl('/login'); // Redirection vers la page de connexion
        });
      },
      (error) => {
        console.log("error")
      }
    );
  }
}
