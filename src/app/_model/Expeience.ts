export class Experience {
    id!: string;
  
    entreprise!: string;
    poste!: string;
    dateDebut!: string;
    dateFin!: string;
    description!: string;
  }