import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CahngePasswordComponent } from './cahnge-password.component';

describe('CahngePasswordComponent', () => {
  let component: CahngePasswordComponent;
  let fixture: ComponentFixture<CahngePasswordComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CahngePasswordComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CahngePasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
