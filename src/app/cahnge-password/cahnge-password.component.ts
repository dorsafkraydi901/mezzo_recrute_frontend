import { Component, OnInit } from '@angular/core';
import { StorageService } from '../_servicesUser/storage.service';
import { UserService } from '../_servicesUser/user.service';
import { HttpClient } from '@angular/common/http';
import { PhotoserviceService } from '../photoservice/photoservice.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AbstractControl, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import Swal from 'sweetalert2';
import { Utilisateur } from '../_servicesUser/utilisateur';

@Component({
  selector: 'app-cahnge-password',
  templateUrl: './cahnge-password.component.html',
  styleUrls: ['./cahnge-password.component.css']
})
export class CahngePasswordComponent implements OnInit {
  editform : any;
  
get username(){
  return this.editform.get('username'); 
}
get adresse(){
  return this.editform.get('adresse'); 
}
get phone(){
  return this.editform.get('phone'); 
}
get date_naiss(){
  return this.editform.get('date_naiss'); 
}
get email(){
  return this.editform.get('email'); 
}
get gender(){
  return this.editform.get('gender'); 
}
get password(){
  return this.editform.get('password'); 
}
file: File | null = null;
files: File | null = null;
  currentUser: any;
  utilisateur : Utilisateur=this.storageService.getUser();
  id ! : number ;
  isSuccessful = false;
  UserService: any;
  isLoggedIn = false;
  showCandidatBoard = false;
  roles: string[] = [];
  constructor(private storageService: StorageService,
    private service:UserService,
    private router: Router,private http: HttpClient,
     private  photoService: PhotoserviceService,
    private route : ActivatedRoute )
     { }

    ngOnInit(): void {
      console.log(this.utilisateur)
      this.editform = new FormGroup({
     
  
        password :new FormControl('',[Validators.minLength(6),Validators.required ]),
      })
    
      //1--> hedhi tchoufelna l utilisateur connecté wala le o tchouf les information te3o zeda valide wale
      this.isLoggedIn = this.storageService.isLoggedIn();

//  2--> keno connecté tjibo b role te3o
      if (this.isLoggedIn) {
        const user = this.storageService.getUser();
        this.roles = user.roles;
        this.showCandidatBoard = this.roles.includes('ROLE_CANDIDAT');
      }

// 3-->ken mouch connecté t5arjelna error
      if (!this.isLoggedIn){
        this.router.navigate(['error']);
      }
      if (!this.showCandidatBoard ){ this.router.navigate(['error']);
    }

    
// 4-->tjibelna les donne ta3 utilisateur el correspond lel id connecté
this.currentUser = this.storageService.getUser();
this.id = this.route.snapshot.params['id'];
this.service.findUserById(this.id.toString()).subscribe(data => {
  this.utilisateur = data;

});
     }

  //  5--> ken mat7ebch tmodifi traj3ek lel profil  
     annuler() {
      this.router.navigate(['profile']);
    }

    // 6--> ken tmodifa traj3ek lel login bech t3awed todkhel b les info jdod
    modifierUser(){
      this.service.updateUser(this.utilisateur).subscribe(data=>{
    
   
        this.storageService.clean();
        Swal.fire({
          position: 'top',
          icon: 'success',
          text: "Votre compte a été mis à jour. Veuillez vous reconnecter.",
          showConfirmButton: false,
          timer: 2000
        })
        this.router.navigateByUrl('/login');
      }
      );
    }}