
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Utilisateur } from '../_servicesOffres/offre';
const AUTH_API = 'http://localhost:8080/api/auth/';
const TOKEN_KEY = 'AuthToken';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})

export class AuthService {
  constructor(private http: HttpClient) {}
  login(email: string, password: string): Observable<any> {
    return this.http.post(
      AUTH_API + 'login',
      {
        email,
        password,
      },
      httpOptions
    );
  }

  register(username: string, email: string,password: string, adresse : string , phone: number, date_naiss: string, gender : string
     ): Observable<any> {
    return this.http.post(
      AUTH_API + 'registre',
      {
        username,
        email,
        password,
        adresse,
        phone,
        date_naiss,
        gender

      },
      httpOptions
    );
  }
  logout(): void{
    sessionStorage.removeItem(TOKEN_KEY)
  }


}

