import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};
@Injectable({
  providedIn: 'root'
})
export class PasswordResetServiceService {
  private REST_PASS = 'http://localhost:8080/api/reset-password';

  constructor(private http: HttpClient) {}

  requestPasswordReset(email: string): Observable<any> {
    const url = `${this.REST_PASS}/request`;
    return this.http.post(url, { email });
  }

  resetPassword(email: string, newPassword: string): Observable<any> {
    const url = `${this.REST_PASS}/reset`;
    return this.http.post(url, { email, newPassword }); // Envoyez les données dans le corps de la requête
  }
  
}
