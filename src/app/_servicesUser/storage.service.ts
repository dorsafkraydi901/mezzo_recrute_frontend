import { Injectable } from '@angular/core';

const USER_KEY = 'auth-user';

@Injectable({
  providedIn: 'root'
})
export class StorageService {
  constructor() {}

 
  clean(): void {
    window.sessionStorage.clear();
    window.sessionStorage.clear();
    window.sessionStorage.clear();
    window.sessionStorage.clear();
    window.sessionStorage.clear();

  }

  public saveUser(user: any): void {
    window.sessionStorage.removeItem(USER_KEY);
    window.sessionStorage.setItem(USER_KEY, JSON.stringify(user));
  }

  public setToken(token: string): void {
    localStorage.setItem(USER_KEY, token);
  }

  public getToken(): string | null {
    return localStorage.getItem(USER_KEY);
  }
  public getUser(): any {
    const user = window.sessionStorage.getItem(USER_KEY);
    if (user) {
      return JSON.parse(user);
    }

    return {};
  }


  public isLoggedIn(): boolean {
    const user = window.sessionStorage.getItem(USER_KEY);
    if (user) {
      return true;
    }

    return false;
  }
  signOut(): void {
    window.sessionStorage.clear();
  }

}
