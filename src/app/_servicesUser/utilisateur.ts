import { Photo } from "../photoservice/photo";

export class Utilisateur {
  id: string = '';
    username !:string;
    email!:string;
    password!:string;
    adresse!:string;
    phone!: number;
    date_naiss!: string;
    gender!: string;
    cv!:string;
    photos!:Photo;
    createdByAdminId!:string;
    roles!: any;
    diplomes!:any;
    supervisor!:any;
  }
 