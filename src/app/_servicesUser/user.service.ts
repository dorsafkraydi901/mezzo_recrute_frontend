import { Utilisateur } from './utilisateur';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';
import { Photo } from '../photoservice/photo';
const API_URL = 'http://localhost:8080/api/test/utilisateurs/';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(private http: HttpClient , private authService: AuthService,) {}


         // EL CRUD
// 1--> tjiblna toul les utilisateurs
  getAllusers() {
    return this.http.get<Utilisateur[]>(API_URL+"getall");
  }

   // 2-->tjib utilisateur wa7ed       
  findUserById(id: string) {
    return this.http.get<Utilisateur>(API_URL + "getuser/" + id);
  }
  
 //  3-->tmodifi les info taa user
 updateUser(u: Utilisateur) {
    return this.http.put<Utilisateur>(API_URL + u.id, u);
}            
  // 4-->najoutiw utilisateur



  








 createUser(username: string, email: string, password: string, 
  adresse: string,phone : number, date_naiss: string ,gender: string, photo:Photo ): Observable<any> {
  const token = sessionStorage.getItem('auth-user');
  let tokenObj: any;
  if (token !== null) {
    tokenObj = JSON.parse(token);
  }
  const authToken = tokenObj.token;
  
  const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + authToken
    })
  };

  const user = {
    username,
    email,
    password,
    adresse,
      phone,
    date_naiss,
    gender,
  
    photo
  };

  return this.http.post(
    API_URL + 'createUser',
     {
      username,
      email,
      password,
      adresse,
      date_naiss,
      gender,
      phone,
      photo

     },
     httpOptions
   );
}
}