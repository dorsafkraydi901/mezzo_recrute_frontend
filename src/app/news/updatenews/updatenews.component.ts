import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ServicesNewsService } from 'src/app/_servicesNews/-services-news.service';
import { ToastrService } from 'ngx-toastr';
import { News } from 'src/app/_servicesNews/News';
import { PhotoserviceService } from 'src/app/photoservice/photoservice.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { StorageService } from 'src/app/_servicesUser/storage.service';


@Component({
  selector: 'app-updatenews',
  templateUrl: './updatenews.component.html',
  styleUrls: ['./updatenews.component.css']
})
export class UpdatenewsComponent implements OnInit {
  id ! : number ;
  imageSelected = false;
  showAdminBoard = false;
  private roles: string[] = [];
  isLoggedIn = false;

  news: any = {
    id:null,
    titre: null,
    datepub: new Date(),
    content: null,
    photos: null,
    utilisateur: null
  };

  editform : any;
  file: File | null = null;
  constructor(private route: ActivatedRoute, private newsService: ServicesNewsService,private storageService: StorageService,
     private toastr: ToastrService, private router: Router ,private photoService : PhotoserviceService) { }
     get titre(){
      return this.editform.get('titre'); 
    }
    get content(){
      return this.editform.get('content'); 
    }
    get photos(){
      return this.editform.get('photos'); 
    }
    ngOnInit(): void {
          
    this.isLoggedIn = this.storageService.isLoggedIn();

    if (this.isLoggedIn) {
      const user = this.storageService.getUser();
      this.roles = user.roles;
      this.showAdminBoard = this.roles.includes('ROLE_ADMIN')|| this.roles.includes('ROLE_RC');
    }
    if (!this.showAdminBoard){ this.router.navigate(['error']);
    }
      this.editform = new FormGroup({
        titre: new FormControl('', [Validators.required]),
        content: new FormControl('', [Validators.required]),
        photos: new FormControl('', [Validators.required, (control) => {
          const file = control.value;
          if (file) {
            const allowedTypes = ['image/jpeg', 'image/png', 'image/gif'];
            if (!allowedTypes.includes(file.type ?? '')) {
              return { invalidFileType: true };
            }
            return null;
          } else {
            return { required: true };
          }
        }]),
      });
    
      const id = this.route.snapshot.params['id'];
      this.newsService.getNewsById(id).subscribe(data => {
        this.news = data;
      });
    }
    

  onSubmit() {
    if (!this.imageSelected) {
      // Display an error message or perform any desired action
      console.log('Please select an image before submitting.');
      return;
    }
    this.newsService.updateNews(this.news.id, this.news).subscribe(
      data => {
        this.onSubmite();
        console.log(data);
        this.toastr.success('News updated successfully!', 'Success', {
          timeOut: 3000,
          progressBar: true,
          progressAnimation: 'increasing'
        });
        this.router.navigate(['/news']);
      },
      error => console.log(error)
    );
  }
  onFileSelected(event: any) {
    this.file = event.target.files[0];
    this.imageSelected = !!this.file;

  }
  onSubmite() {
    if (!this.file) {
      return;
    }
  
    const allowedTypes = ['image/jpeg', 'image/png', 'image/gif'];
    if (!allowedTypes.includes(this.file.type ?? '')) {
      console.error('Invalid file type. Only JPEG, PNG, and GIF images are allowed.');
      return;
    }
  

    const id = this.route.snapshot.params['id'];
    this.newsService.getNewsById(id).subscribe(data => {
      this.news = data;
      // this.editform.patchValue(this.news);
    });
  
    const formData = new FormData();
    formData.append('image', this.file);
  
    this.photoService.uploadImagenews(this.news.id, formData).subscribe(
      () => {
        console.log('Image uploaded successfully');
      },
      error => {
        console.error('Error uploading image:', error);
      }
    );
  }
}
