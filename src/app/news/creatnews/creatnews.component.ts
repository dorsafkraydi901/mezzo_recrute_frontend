import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ServicesNewsService } from 'src/app/_servicesNews/-services-news.service';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { StorageService } from 'src/app/_servicesUser/storage.service';

@Component({
  selector: 'app-creatnews',
  templateUrl: './creatnews.component.html',
  styleUrls: ['./creatnews.component.css']
})
export class CreatnewsComponent implements OnInit {
  news! : any;
  loginform:any;
  form: any = {
    titre: null,
    content: null,
    
  };
  id ! : number ;
  showAdminBoard = false;
  private roles: string[] = [];
  isLoggedIn = false;

  constructor(private newsService: ServicesNewsService,   private toastr: ToastrService,private dialog: MatDialog,private storageService: StorageService,
    private router: Router
    ) { }
    dialogRef!: MatDialogRef<any>;
  ngOnInit(): void {
    
    this.isLoggedIn = this.storageService.isLoggedIn();

    if (this.isLoggedIn) {
      const user = this.storageService.getUser();
      this.roles = user.roles;
      this.showAdminBoard = this.roles.includes('ROLE_ADMIN')|| this.roles.includes('ROLE_RC');
    }
    if (!this.showAdminBoard){ this.router.navigate(['error']);
    }


    this.loginform = new FormGroup({

      "titre": new FormControl(null, [Validators.required]),
      "content"  :new FormControl(null,[Validators.required ])
    })
  }
  get titre(){
    return this.loginform.get('titre'); 
  }
  get content(){
    return this.loginform.get('content'); 
  }
  onSubmit() {
    const { titre, content } = this.form;
    this.newsService.createNews(titre, content).subscribe(
      data => {
        console.log(data);
        // navigate to the update news page
        this.router.navigate(['/news', data.id]);
      },
      error => console.log(error)
    );
  }
  
  
  
}
