import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatnewsComponent } from './creatnews.component';

describe('CreatnewsComponent', () => {
  let component: CreatnewsComponent;
  let fixture: ComponentFixture<CreatnewsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreatnewsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CreatnewsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
