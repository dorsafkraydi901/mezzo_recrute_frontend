import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServicesNewsService } from 'src/app/_servicesNews/-services-news.service';
import { News } from 'src/app/_servicesNews/News';
import { StorageService } from 'src/app/_servicesUser/storage.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-getall',
  templateUrl: './getall.component.html',
  styleUrls: ['./getall.component.css']
})
export class GetallComponent implements OnInit {
  newsList: News[] = [];
  selectedNews: News | null = null;
  currentPage = 1;
  itemsPerPage = 6;
  showAdminBoard = false;
  private roles: string[] = [];
  isLoggedIn = false;
news: any;
  get totalPages(): number {
    return Math.ceil(this.newsList.length / this.itemsPerPage);
  }

  get pagedNewsList(): News[] {
    const startIndex = (this.currentPage - 1) * this.itemsPerPage;
    const endIndex = startIndex + this.itemsPerPage;
    return this.newsList.slice(startIndex, endIndex);
  }
  
  constructor(private newsService: ServicesNewsService,private storageService: StorageService, private router: Router) { }

  ngOnInit(): void {
    this.getAllNews();
    this.isLoggedIn = this.storageService.isLoggedIn();

    if (this.isLoggedIn) {
      const user = this.storageService.getUser();
      this.roles = user.roles;

      this.showAdminBoard = this.roles.includes('ROLE_ADMIN');
 
    }
  
  }

  getAllNews() {
    this.newsService.getAllNews().subscribe((data: News[]) => {
      this.newsList = data;

      // Sort the news list in descending order by datepub
      this.newsList.sort((a, b) => new Date(b.datepub).getTime() - new Date(a.datepub).getTime());

      // Show the first news item by default
      this.selectedNews = this.newsList[0];
    });
  }

  showSingleNews(id: string) {
    this.router.navigate(['detailnews',id]);
    this.newsService.getNewsById(id).subscribe((data: News) => {
      if (data !== undefined) {
        this.selectedNews = data;
      }
    });
  }

  goToPage(pageNumber: number) {
    this.currentPage = pageNumber;
  }
  deleteNews(id: string) {
    Swal.fire({
      title: 'Confirmation de suppression ',
      text: 'Êtes-vous sûr de vouloir supprimer cette actualité ?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '  #d33',
      cancelButtonColor: '#3085d6',
      confirmButtonText: 'Supprimer',
      cancelButtonText: 'Annuler',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        this.newsService.deleteNews(id).subscribe(() => {
          Swal.fire('Supprimée !', 'L\'actualité a été supprimée avec succès.', 'success');
          this.getAllNews();
        });
      }
    });
  }
}





