import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ServicesNewsService } from 'src/app/_servicesNews/-services-news.service';
import { News } from 'src/app/_servicesNews/News';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {
  news: News | undefined;
  otherNews: News[] = [];

  constructor(private route: ActivatedRoute, private newsService: ServicesNewsService) { }

  ngOnInit(): void {
    const newsId = this.route.snapshot.params['id'];
    this.loadNewsDetails(newsId);
    this.loadOtherNews();
  }

  loadNewsDetails(newsId: string): void {
    this.newsService.getNewsById(newsId).subscribe(
      (data: News) => {
        this.news = data;
      },
      (error) => {
        console.log('Error occurred while fetching news details:', error);
      }
    );
  }

  loadOtherNews() {
    // Get the list of other news items
    this.newsService.getAllNews().subscribe(data => {
      this.otherNews = data;
    });
  }

  showSingleNews(newsId: string) {
    // Load the clicked news item in the main div
    this.loadNewsDetails(newsId);
  }
}
