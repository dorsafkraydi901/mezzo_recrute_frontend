import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup,FormControl, Validators } from '@angular/forms';
import { AuthService } from '../_servicesUser/auth.service';
import { StorageService } from '../_servicesUser/storage.service';


@Component({
  selector: 'app-login-emp',
  templateUrl: './login-emp.component.html',
  styleUrls: ['./login-emp.component.css']
})
export class LoginEmpComponent implements OnInit {
  loginform:any;
  isSuccessful = false;
  form: any = {
    matriculerh: null,
    password: null
  };
  isLoggedIn = false;
  isLoginFailed = false;
  nom="";
  errorMessage = '';
  roles: string[] = [];

  constructor(private authService: AuthService, private storageService: StorageService, private router: Router) { }

  ngOnInit(): void {
    this.isLoggedIn = this.storageService.isLoggedIn();

    if (this.isLoggedIn) {
        this.router.navigate(['error']);

    }
    this.loginform = new FormGroup({

      "matriculerh": new FormControl(null, [Validators.required, Validators.pattern('[0-9]*'), Validators.minLength(5), Validators.maxLength(5)]),
      "password"  :new FormControl(null,[Validators.minLength(6),Validators.required ])
    })
  }
    // 1-->login with google
    // signInWithGoogle(): void {
    //   this.autheService.signIn(GoogleLoginProvider.PROVIDER_ID);
    // }
    
  onSubmit(): void {
    
    const { matriculerh, password } = this.form;
    this.authService.login(matriculerh, password).subscribe({

      next: data => {
        this.storageService.saveUser(data);
        this.router.navigateByUrl('/acceuil').then(this.reloadPage);
        this.isLoginFailed = false;
        
        this.isLoggedIn = true;
        this.roles = this.storageService.getUser().roles;
        
    
      },
      error: err => {
        this.errorMessage = "Échec de connexion.";
        this.isLoginFailed = true;
      }
    });
  }
  reloadPage(): void {
   window.location.reload();
  }
  get matriculerh(){
  return this.loginform.get('matriculerh'); 
}
get password(){
  return this.loginform.get('password'); 
}

}
