import { Component, OnInit } from '@angular/core';
import { ServiceDemandeService } from 'src/app/_servicesDemande/-service-demande.service';
import { Demande } from 'src/app/_servicesDemande/demande';
import { DatePipe } from '@angular/common';
import { Router } from '@angular/router';
import { StorageService } from 'src/app/_servicesUser/storage.service';

@Component({
  selector: 'app-deman-valide',
  templateUrl: './deman-valide.component.html',
  styleUrls: ['./deman-valide.component.css']
})
export class DemanValideComponent implements OnInit {
  demandes: Demande[] | undefined;
  isLoggedIn = false;
  showRCBoard = false;
  private roles: string[] = [];
  showRDBoard = false;

  constructor(private demandeService: ServiceDemandeService ,private storageService: StorageService,     private router: Router) { }


  ngOnInit(): void {
    this.isLoggedIn = this.storageService.isLoggedIn();

    if (this.isLoggedIn) {
      const user = this.storageService.getUser();
      this.roles = user.roles;
      this.showRCBoard = this.roles.includes('ROLE_RC') || this.roles.includes('ROLE_RECRUTEUR');

    }
    if (!this.showRCBoard){ this.router.navigate(['error']);
    }

    this.getValideDemandes();
  }
  getValideDemandes(): void {
    this.demandeService.getValideDemandes()
    
      .subscribe(
        demandes => this.demandes = demandes);
  }
  detail(id : string){
    this.router.navigate(['ddemande',id]);
  }
}
