import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DemanValideComponent } from './deman-valide.component';

describe('DemanValideComponent', () => {
  let component: DemanValideComponent;
  let fixture: ComponentFixture<DemanValideComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DemanValideComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DemanValideComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
