import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ServiceDemandeService } from 'src/app/_servicesDemande/-service-demande.service';
import { StorageService } from 'src/app/_servicesUser/storage.service';

@Component({
  selector: 'app-demandedetail',
  templateUrl: './demandedetail.component.html',
  styleUrls: ['./demandedetail.component.css']
})
export class DemandedetailComponent implements OnInit {
  demande! : any;
  constructor(
    private router: Router,private route: ActivatedRoute,private storageService: StorageService,
    private demandeService: ServiceDemandeService
  ) { }
  showRDBoard = false;
  private roles: string[] = [];
  isLoggedIn = false;
  ngOnInit(): void {
    this.isLoggedIn = this.storageService.isLoggedIn();

    if (this.isLoggedIn) {
      const user = this.storageService.getUser();
      this.roles = user.roles;
      this.showRDBoard = this.roles.includes('ROLE_RD');
     
    }
    if (!this.showRDBoard ){ this.router.navigate(['error']);
    }
    const id = this.route.snapshot.params['id'];
    this.demandeService.findDemandeById(id).subscribe(data => {
      this.demande = data;
           this.demande = data;
           this.demande = data.sort((a, b) => {
            return new Date(b.date_demande).getTime() - new Date(a.date_demande).getTime();
          });
          this.demande = data.sort((a, b) => {
            return new Date(b.date_integration).getTime() - new Date(a.date_integration).getTime();
          });
         });
  }
  retour() {
    this.router.navigate(['demandes']);
  }
}
