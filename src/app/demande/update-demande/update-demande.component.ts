import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ServiceDemandeService } from 'src/app/_servicesDemande/-service-demande.service';
import { Demande } from 'src/app/_servicesDemande/demande';
import { StorageService } from 'src/app/_servicesUser/storage.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-update-demande',
  templateUrl: './update-demande.component.html',
  styleUrls: ['./update-demande.component.css']
})
export class UpdateDemandeComponent implements OnInit {

  constructor(private demandeService: ServiceDemandeService, 
    private router: Router, 
    private storageService: StorageService,
     private route: ActivatedRoute,
     private formBuilder: FormBuilder) { }
  demande: Demande = new Demande();
  showRDBoard = false;
  submitted = false;
  isLoggedIn = false;
  showSpinner: boolean = false;
  private roles: string[] = [];
  isSuccessful = false;
  isSignUpFailed = false
  demandeEditform!: FormGroup;

  // demandeEditform = new FormGroup({
  //   // site: new FormControl('', [Validators.required]),
  //   date_demande: new FormControl('', [Validators.required]),
  //   date_integration: new FormControl('', [Validators.required]),
  //   expiration: new FormControl('', [Validators.required]),
  //   // type_contrat: new FormControl(null, [Validators.required]),
  //   nbr_collab: new FormControl('', [Validators.required]),
  //   // temps_travail: new FormControl('', [Validators.required]),
  //   profile_chercher: new FormControl('', [Validators.required]),
  //   poste_recherche: new FormControl('', [Validators.required,]),
  //   desciption: new FormControl('', [Validators.required,])
  // });
  // get date_demande() {
  //   return this.demandeEditform.get('date_demande');
  // }
  // get date_integration() {
  //   return this.demandeEditform.get('date_integration');
  // }
  // get nbr_collab() {
  //   return this.demandeEditform.get('nbr_collab');
  // }
  // get profile_chercher() {
  //   return this.demandeEditform.get('profile_chercher');
  // }
  // get poste_recherche() {
  //   return this.demandeEditform.get('poste_recherche');
  // }
  // get desciption() {
  //   return this.demandeEditform.get('desciption');
  // }
  // get expiration() {
  //   return this.demandeEditform.get('expiration');
  // }

  ngOnInit() {
this.demandeEditform = this.formBuilder.group({
  site: ['', Validators.required],
  date_integration: ['', Validators.required],
  type_contrat: ['', Validators.required],
  nbr_collab: ['', Validators.required],
  expiration: ['', Validators.required , dateFinValidator],
  temps_travail: ['', Validators.required],
  profile_chercher: ['', Validators.required],
  poste_recherche: ['', Validators.required],
  description: [''],
});

  
    this.isLoggedIn = this.storageService.isLoggedIn();

    if (this.isLoggedIn) {
      const user = this.storageService.getUser();
      this.roles = user.roles;
      this.showRDBoard = this.roles.includes('ROLE_RD');

    }
    if (!this.showRDBoard) {
      this.router.navigate(['error']);
    }
    const id = this.route.snapshot.params['id'];
    this.demandeService.findDemandeById(id).subscribe(data => {
      this.demande = data;
      this.demandeEditform.patchValue(this.demande);
    });

  }
  get site(){
    return this.demandeEditform.get('site'); 
  }
  get nbr_collab() {
    return this.demandeEditform.get('nbr_collab');
  }

onSubmit(){

  this.demandeService.updatedemande(this.demande).subscribe(
    
    data => {
  
      Swal.fire({
        position: 'top',
        icon: 'success',
        text: "Le demande a été modifiée avec succès.",
        showConfirmButton: false,
        timer: 2000
      })
      this.router.navigate(['/demandes']);
    },
    error => {
      console.log(error);
    }
  );
}
isValidDateRange(): boolean {
  const dateDebut = new Date(this.demande.date_integration);
  const dateFin = new Date(this.demande.expiration);
  return dateDebut < dateFin;
}}
function dateFinValidator(control: AbstractControl): ValidationErrors | null {
  const expiration = control.value;
  const date_integration = control.root.get('date_recrut')?.value;
  if (date_integration && expiration && expiration < date_integration) {
    return { dateFinBeforeDebut: true };
  }
  return null;
}
