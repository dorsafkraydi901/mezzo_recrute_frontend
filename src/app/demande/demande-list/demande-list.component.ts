import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServiceDemandeService } from 'src/app/_servicesDemande/-service-demande.service';
import { StorageService } from 'src/app/_servicesUser/storage.service';

@Component({
  selector: 'app-demande-list',
  templateUrl: './demande-list.component.html',
  styleUrls: ['./demande-list.component.css']
})
export class DemandeListComponent implements OnInit {

  demande! : any;
  filterText: string = '';
  filterValide: boolean = false;
  filterEnAttente: boolean = false;
  filterRefuse: boolean = false;
 etatDemande: string = '';
  constructor(
    private router: Router,
    private demandeService: ServiceDemandeService,private storageService: StorageService,
  ) { }
  showAdminBoard = false;
  private roles: string[] = [];
  isLoggedIn = false;

  ngOnInit(): void {

    this.isLoggedIn = this.storageService.isLoggedIn();

    if (this.isLoggedIn) {
      const user = this.storageService.getUser();
      this.roles = user.roles;
      this.showAdminBoard = this.roles.includes('ROLE_ADMIN')|| this.roles.includes('ROLE_RC');
    }
    if (!this.showAdminBoard && !this.showAdminBoard){ this.router.navigate(['error']);
    }



    this.loadOffres();
  }
  // methode hedhy tjibli f all demandes ml base
  loadOffres(){
    this.demandeService.getAllDemmandes().subscribe((data) =>{
      this.demande = data.sort((a, b) => {
        return new Date(b.date_demande).getTime() - new Date(a.date_demande).getTime();
      });
      this.demande=data;
    });
  }
// methode m3aytelha f button ta3 details taba3thlk l demande bl id te3o
//    hadheka bedhet w tshouf details te3o
  detail(id : string){
    this.router.navigate(['detaile',id]);
  }


  
    filterDemandes() {
    let filteredDemandes = [...this.demande];

    const filterFunctions = [];

    if (this.filterValide) {
      filterFunctions.push((demande: any) => {
        return demande.validation.toLowerCase() === 'valide';
      });
    }
    if (this.filterEnAttente) {
      filterFunctions.push((demande: any) => {
        return demande.validation.toLowerCase() === 'en_attente';
      });
    }
    if (this.filterRefuse) {
      filterFunctions.push((demande: any) => {
        return demande.validation.toLowerCase() === 'refuse';
      });
    }

    if (filterFunctions.length > 0) {
      filteredDemandes = filterFunctions.reduce((prev, current) => {
        return prev.filter(current);
      }, filteredDemandes);
    }

    if (this.filterText) {
      filteredDemandes = filteredDemandes.filter((demande: any) =>
        demande.utilisateur.username.toLowerCase().includes(this.filterText.toLowerCase()) ||
        demande.utilisateur.email.toLowerCase().includes(this.filterText.toLowerCase())
      );
    }

    if (this.etatDemande) {
      filteredDemandes = filteredDemandes.filter((demande: any) => {
        return demande.validation.toLowerCase() === this.etatDemande.toLowerCase();
      });
    }

    return filteredDemandes;
  }

  // method to check if only one checkbox is selected
  isOneCheckboxSelected() {
    return (
      (this.filterValide && !this.filterEnAttente && !this.filterRefuse) ||
      (!this.filterValide && this.filterEnAttente && !this.filterRefuse) ||
      (!this.filterValide && !this.filterEnAttente && this.filterRefuse)
    );
  }
}