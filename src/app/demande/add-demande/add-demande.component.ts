import {  HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup,FormControl, Validators, AbstractControl, ValidationErrors } from '@angular/forms';

import { ServiceDemandeService } from 'src/app/_servicesDemande/-service-demande.service';
import { Demande, Validation } from 'src/app/_servicesDemande/demande';
import { StorageService } from 'src/app/_servicesUser/storage.service';
@Component({
  selector: 'app-add-demande',
  templateUrl: './add-demande.component.html',
  styleUrls: ['./add-demande.component.css']
})
export class AddDemandeComponent implements OnInit {
  showRDBoard = false;
  showSpinner: boolean = false;
  private roles: string[] = [];
  isLoggedIn = false;
  demande: Demande = new Demande();
  demandeform = new FormGroup({
    // site: new FormControl('', [Validators.required]),
    date_demande: new FormControl('', [Validators.required]),
    date_integration: new FormControl('', [Validators.required]),
    expiration: new FormControl('', [Validators.required , dateFinValidator]),
    // type_contrat: new FormControl(null, [Validators.required]),
    nbr_collab: new FormControl('', [Validators.required]),
    // temps_travail: new FormControl('', [Validators.required]),
    profile_chercher: new FormControl('', [Validators.required]),
    poste_recherche: new FormControl('', [Validators.required ,]),
    desciption: new FormControl('', [Validators.required ,])
  });
  get date_demande(){
    return this.demandeform.get('date_demande'); 
  }
  get date_integration(){
    return this.demandeform.get('date_integration'); 
  }
  get nbr_collab(){
    return this.demandeform.get('nbr_collab'); 
  }
  get profile_chercher(){
    return this.demandeform.get('profile_chercher'); 
  }
  get poste_recherche(){
    return this.demandeform.get('poste_recherche'); 
  }
  get desciption(){
    return this.demandeform.get('desciption'); 
  }
  get expiration(){
    return this.demandeform.get('expiration'); 
  }
  isSignUpFailed = false

  showRRHPage = false;
  type_contrat: string | undefined ; 
  form: any = {
    site: null,
    date_demande: null,
    date_integration: null,
    type_contrat: null,
    nbr_collab: null,
    temps_travail: null,
    profile_chercher: null,
    poste_recherche: null,
    description: null,
    expiration : null,
    validation: Validation.EN_ATTENTE,
    Utilisateur : null,
  };
  isSuccessful = false;
  errorMessage = '';
  temps_travail: any;
  site: any;
  constructor(
    private router: Router,
    private demandeService: ServiceDemandeService,  private storageService: StorageService,
  ) { }

  ngOnInit() {
  
    this.isLoggedIn = this.storageService.isLoggedIn();

    if (this.isLoggedIn) {
      const user = this.storageService.getUser();
      this.roles = user.roles;
      this.showRDBoard = this.roles.includes('ROLE_RD');
     
    }
    if (!this.showRDBoard ){ this.router.navigate(['error']);
    }

  }
  onSubmit() : void {
    this.showSpinner = true;
    // hedhom l fazet select option f formulaire takra w tkayed l value 
    this.type_contrat = this.form.type_contrat.value; 
    this.temps_travail = this.form.temps_travail.value; 
    this.site = this.form.site.value; 
    // tjibli f token m sessionstorage ta3 shkoun halel waketha 
    const token = sessionStorage.getItem('auth-user');
    let tokenObj : any; 
     if (token !== null) {
       tokenObj =  JSON.parse(token);
     }
  
     const authToken = tokenObj.token;
    const {site,date_demande, date_integration, type_contrat, 
      nbr_collab, temps_travail, profile_chercher, poste_recherche,expiration, 
      description , validation , utilisateur} = this.form;
      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + authToken
          
        })
      };
      
    this.demandeService.createDemande(site,date_demande, date_integration, type_contrat, 
      nbr_collab, temps_travail, profile_chercher, poste_recherche,expiration, description , validation , utilisateur,httpOptions ).subscribe({
        next: data => {
          console.log(data);
          this.router.navigateByUrl('/demandes');
          this.isSignUpFailed = false;
        },
        error: err => {
          this.errorMessage = "Échec de l'ajout";
          
          this.isSignUpFailed = true;
        }

      });
  }
  isValidDateRange(): boolean {
    const dateDebut = new Date(this.demande.date_integration);
    const dateFin = new Date(this.demande.expiration);
    return dateDebut < dateFin;
  }}

function dateFinValidator(control: AbstractControl): ValidationErrors | null {
  const expiration = control.value;
  const date_integration = control.root.get('date_recrut')?.value;
  if (date_integration && expiration && expiration < date_integration) {
    return { dateFinBeforeDebut: true };
  }
  return null;
}
