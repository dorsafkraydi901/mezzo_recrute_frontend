import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ServiceDemandeService } from 'src/app/_servicesDemande/-service-demande.service';
import { Demande, Validation } from 'src/app/_servicesDemande/demande';
import { StorageService } from 'src/app/_servicesUser/storage.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-demande-details',
  templateUrl: './demande-details.component.html',
  styleUrls: ['./demande-details.component.css']
})
export class DemandeDetailsComponent implements OnInit {
  showAdminBoard = false;
  showSpinner: boolean = false;
  private roles: string[] = [];
  isLoggedIn = false;
  demande! : any;
  demandes! : Demande;
  id!: string;
  // hedhom ta3 validation radio 
  validationOptions = [
    {
      name: 'En attente',
      value: Validation.EN_ATTENTE
    },
    {
      name: 'Refusé',
      value: Validation.REFUSE
    },
    {
      name: 'Validé',
      value: Validation.VALIDE
    }
  ];
Validation: any;
  constructor(
    private router: Router,private route: ActivatedRoute,
    private demandeService: ServiceDemandeService,private storageService: StorageService,
  ) { }
 
  ngOnInit(): void {

    this.isLoggedIn = this.storageService.isLoggedIn();

    if (this.isLoggedIn) {
      const user = this.storageService.getUser();
      this.roles = user.roles;
      this.showAdminBoard = this.roles.includes('ROLE_ADMIN')|| this.roles.includes('ROLE_RC');
    }
    if (!this.showAdminBoard){ this.router.navigate(['error']);
    }



    // Récupérer l'demande à modifier en fonction de l'ID
  const id = this.route.snapshot.params['id'];
  this.demandeService.findDemandeById(id).subscribe(data => {
    this.demande = data;
         this.demande = data;
         this.demande = data.sort((a, b) => {
          return new Date(b.date_demande).getTime() - new Date(a.date_demande).getTime();
        });
        this.demande = data.sort((a, b) => {
          return new Date(b.date_integration).getTime() - new Date(a.date_integration).getTime();
        });
       });
     }
     
  // methode hedhy 3maltha yestaamel feha l admin besh ynajem yupdati l etat ta3 demande
  updateDeamandes(){
    this.showSpinner = true;
    this.demandeService.updateDeamandes(this.demande).subscribe(
      
      data => {
    
        Swal.fire({
          position: 'top',
          icon: 'success',
          text: "Le demande a été modifiée avec succès.",
          showConfirmButton: false,
          timer: 2000
        })
        this.router.navigate(['/demandeslist']);
      },
      error => {
        console.log(error);
      }
    );
  }
  

 
}
