import { HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServiceDemandeService } from 'src/app/_servicesDemande/-service-demande.service';
import { Demande } from 'src/app/_servicesDemande/demande';
import { StorageService } from 'src/app/_servicesUser/storage.service';

@Component({
  selector: 'app-demandes',
  templateUrl: './demandes.component.html',
  styleUrls: ['./demandes.component.css']
})
export class DemandesComponent implements OnInit {
  demande! : any;
  filterText: string = '';
  showRDBoard = false;
  private roles: string[] = [];
  isLoggedIn = false;
  constructor(
    private router: Router,
    private storageService: StorageService,
    private demandeService: ServiceDemandeService
  ) { }
  ngOnInit(): void {
    this.isLoggedIn = this.storageService.isLoggedIn();

    if (this.isLoggedIn) {
      const user = this.storageService.getUser();
      this.roles = user.roles;
      this.showRDBoard = this.roles.includes('ROLE_RD');
     
    }
    if (!this.showRDBoard ){ this.router.navigate(['error']);
    }


    this.loadOffres();
   
  }
  loadOffres(){

    this.demandeService.getDemandesForUtilisateur().subscribe(
      (data: Demande[]) => {
        this.demande = data;
      },
      (error) => {
        console.log(error);
      });
  }
  // for le button voir le details button send you to the detail of one demande
  detail(id : string){
    this.router.navigate(['detai',id]);
  }
  updatedemande(id:string){
    this.router.navigate(['editDemande',id]);
  }
  filterDemandes() {
    if (!this.demande) {
      return [];
    }
    if (!this.filterText) {
      return this.demande;
    }
    return this.demande.filter((demande: any) =>
    demande.site.toLowerCase().includes(this.filterText.toLowerCase())||
    demande.poste_recherche.toLowerCase().includes(this.filterText.toLowerCase()) ||
    demande.type_contrat.toLowerCase().includes(this.filterText.toLowerCase()) ||
    demande.validation.toLowerCase().includes(this.filterText.toLowerCase())
     
      );
}

}