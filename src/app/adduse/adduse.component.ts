import { Component, OnInit } from '@angular/core';
import { Utilisateur } from '../_servicesUser/utilisateur';
import { UserService } from '../_servicesUser/user.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { AbstractControl, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { StorageService } from '../_servicesUser/storage.service';

@Component({
  selector: 'app-adduse',
  templateUrl: './adduse.component.html',
  styleUrls: ['./adduse.component.css']
})
export class AdduseComponent implements OnInit {
  registreform: any;

  form: any = {
    username: null,
    email: null,
    password: null,
    adresse: null,
    phone: null,
    date_naiss: null,
    gender: null,
    photo:null
  };
  isSuccessful = false;
  isSignUpFailed = false;
  errorMessage = '';
  isLoggedIn = false;
  showRCBoard = false;
  private roles: string[] = [];
  utilisateur: Utilisateur = new Utilisateur();
  
  constructor(private userService: UserService , private router: Router, private storageService: StorageService ) { }

  ngOnInit(): void {
    this.isLoggedIn = this.storageService.isLoggedIn();

    if (this.isLoggedIn) {
      const user = this.storageService.getUser();
      this.roles = user.roles;
      this.showRCBoard = this.roles.includes('ROLE_RC') || this.roles.includes('ROLE_RECRUTEUR');
    }
    if (!this.showRCBoard){ this.router.navigate(['error']);
    }

       // validation for the form 
       this.registreform = new FormGroup({
        "username": new FormControl(null, [Validators.required,Validators.minLength(3),Validators.maxLength(10)]),
        "adresse": new FormControl(null, [Validators.required, Validators.pattern(/^[a-zA-Z\s]*$/)]),
        "email": new FormControl(null, [Validators.required, Validators.email]),
        "password": new FormControl(null, [Validators.minLength(6), Validators.required]),
        "phone": new FormControl(null, [Validators.required, Validators.pattern("^[0-9]+")]),
        "date_naiss": new FormControl(null, [Validators.required, under18Validator()]),
        "gender": new FormControl(null, [Validators.required]),
  
  
      })
      this.isLoggedIn = this.storageService.isLoggedIn();
  
      if (!this.isLoggedIn) {
          this.router.navigate(['error']);
  
      }
  
  }

  get username() {
    return this.registreform.get('username');
  }
  get adresse() {
    return this.registreform.get('adresse');
  }
  
  get email() {
    return this.registreform.get('email');
  }
  
  get password() {
    return this.registreform.get('password');
  }
    
  get phone() {
    return this.registreform.get('phone');
  }
    
  get date_naiss() {
    return this.registreform.get('date_naiss');
  }
  get gender() {
    return this.registreform.get('gender');
  }


  onSubmit(): void {
console.log("akre")
    const { username, email, password, adresse, phone, date_naiss,gender,photo } = this.form;

    this.userService.createUser(username, email, password, adresse, phone, date_naiss,gender,photo ).subscribe({
      next: data => {
        console.log(data);
        if (this.isSuccessful = true)
          Swal.fire({
            position: 'top',
            icon: 'success',
            confirmButtonColor: '#25377A',
            title: "Félicitation! Votre Ajouter un candidat est réussie.",
            showConfirmButton: true,
          })
          this.router.navigateByUrl('/acceuil');

        this.isSignUpFailed = false;
      },
      error: err => {
        this.errorMessage = "Échec de l'inscription.";

        this.isSignUpFailed = true;
      }
    });
  }
  isUnderAge(): boolean {
    const now = new Date();
    const birthDate = new Date(this.date_naiss.value);
    let age = now.getFullYear() - birthDate.getFullYear();
    const monthDiff = now.getMonth() - birthDate.getMonth();
    if (monthDiff < 0 || (monthDiff === 0 && now.getDate() < birthDate.getDate())) {
      age--;
    }
    return age < 18 || age >= 45;
  }
}
export function under18Validator(): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } | null => {
    const birthdate = new Date(control.value);
    const today = new Date();
    let age = today.getFullYear() - birthdate.getFullYear();
    const monthDiff = today.getMonth() - birthdate.getMonth();
    if (monthDiff < 0 || (monthDiff === 0 && today.getDate() < birthdate.getDate())) {
      age--;
    }
    return age < 18 || age > 45 ? { under18: true } : null;
  };
}
