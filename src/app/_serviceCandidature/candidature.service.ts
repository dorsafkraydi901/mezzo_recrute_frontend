import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Candidature } from './Candidature';
import { Utilisateur } from '../_servicesUser/utilisateur';

const API_URL = 'http://localhost:8080/api/test/candidature/';

@Injectable({
  providedIn: 'root'
})
export class CandidatureService {
  httpHeaders = new HttpHeaders({
    'Content-Type': 'application/json',
    'Authorization': 'Bearer ' + localStorage.getItem('authToken') // récupérer le jeton de l'utilisateur
  });

  constructor(private http: HttpClient) { }
// EL CRUD
     //tjiblna candidature taa candidat sur un offre specifier
     getCandidaturesByCandidatAndOffre(offreId: string, utilisateurId: string): Observable<Candidature[]> {
      return this.http.get<Candidature[]>(`${API_URL}candidatures/offre/${offreId}/utilisateur/${utilisateurId}`, { headers: this.httpHeaders });  
      }
      // 1--> tjiblna toul les candidature
     getAllcandidature() {
      return this.http.get<Candidature[]>(API_URL+"getall");
    }
  
    getCandidateursByOffre(offreId: string) :Observable<Candidature[]>{
      return this.http.get<Candidature[]>(API_URL+"offre/" + offreId + "/candidateurs");
    }

       // 2-->tjib candidature wa7da      
       findCandidatureById(id: string) {
    return this.http.get<Candidature>(API_URL + "getcandidature/" + id);
  }
  
  updateAllCandidtaure(c: Candidature) {
    return this.http.put<Candidature>(API_URL + c.id, c);
} 
  //3-->cree un candidature

  createCandidature(candidature: Candidature, httpOptions: any): Observable<any> {
    return this.http.post(API_URL + "addcandidature", candidature, httpOptions);
}

getCandidaturesByCandidat(candidatId: string): Observable<Candidature[]> {
  return this.http.get<Candidature[]>(API_URL + 'candidat/' + candidatId + '/candidatures');
}
getCandidaturesForUtilisateurAndOffre(utilisateurId: string, offreId: string): Observable<Candidature[]> {
  
  return this.http.get<Candidature[]>(API_URL + utilisateurId + '/a/' +   offreId + '/c');
}


getCandidaturesForUtilisateur(utilisateurId: string): Observable<Candidature[]> {
  return this.http.get<Candidature[]>(API_URL + utilisateurId + '/c');
}


//tjibli les candidatures accepté
getAllCandidaturesAcceptees(): Observable<Candidature[]> {
  return this.http.get<Candidature[]>(API_URL+"getall/accepte");
 
}
}