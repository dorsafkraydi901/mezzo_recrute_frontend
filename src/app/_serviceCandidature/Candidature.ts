import { offre } from "../_servicesOffres/offre";
import { Utilisateur } from "../_servicesUser/utilisateur";


export class Candidature {
    id!: string;
    date_depot!: Date;
    etat!: string;
    blocklist!: string;
    remarque!:string;
    canaux!:string;
    utilisateur: Utilisateur = new Utilisateur();
   
    diplome!: {
      titre: string;
      etablissement: string;
      annee: string;
    };
    experience!: {
      entreprise: string;
      poste: string;
      dateDebut: string;
      dateFin: string;
      description: string;
    };
    offres!: offre; 
offreId: any;

  }