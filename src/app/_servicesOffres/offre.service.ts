import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { offre } from './offre';
import { Utilisateur } from '../_servicesUser/utilisateur';

const API_URL = 'http://localhost:8080/api/test/offres/';
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
@Injectable({
  providedIn: 'root'
})
export class OffreService {
  [x: string]: any;

  constructor(private http: HttpClient) { }
       // EL CRUD
 // desactiver les offre
 disableOffre(id: string, ): Observable<any> {
  return this.http.put<string>(`${API_URL}${id}/desactiver`, {});
}
// activer les offre
enableOffre(id: string, ): Observable<any> {
  return this.http.put<string>(`${API_URL}${id}/activer`, {});
}
        // 1--> tjiblna toul les offres
     getAlloffres() {
      
      return this.http.get<offre[]>(API_URL+"getall");
    }
     // 1--> tjiblna tous les offres interne akahaw
    getOffresInterne() {
      return this.http.get<offre[]>(API_URL+"offreinterne");
    }
     // 1--> tjiblna tous les offres externe akahaw
     
    getOffresExterne() {
      return this.http.get<offre[]>(API_URL+"offreexterne");
    }
    getOffresTous() {
      return this.http.get<offre[]>(API_URL+"offretous");
    }
 
       // 2-->tjib utilisateur wa7ed       
  findOffreById(id: string) {
    return this.http.get<offre>(API_URL + "getoffre/" + id);
  }
  
 //  3-->tmodifi les info taa user
 updateAllOffre(o: offre) {
    return this.http.put<offre>(API_URL + "Hide/"+ o.id, o);
} 

//4-->cree un offre qui correspond a leur demande
addoffre(titre: string, lieu: string,description: string, type_poste : string , type_contrat: string, 
  competence: string, regime : string, date_recrut : String, date_fin : String,typeOffre:string, utilisateur : Utilisateur 
  ,  httpOptions: any): Observable<any> {
    // const date_publication = new Date();
 return this.http.post(
  API_URL + 'addoffre',
   {
    titre,
    lieu,
    description,
    type_poste,
    type_contrat,
    competence,
    regime,
    date_recrut,
    date_fin,
    typeOffre,
    // date_publication,
    utilisateur ,
   },
   httpOptions
 );
}

//5-->tfasakh offre
deleteOffre(id : string){
  return this.http.delete<offre[]>(API_URL+id);
}
updateAllOffreToHide(id: string, o: offre): Observable<offre> {
  const updatedOffre = { ...o, hide: true }; // Set hide to true
  return this.http.put<offre>(`${API_URL}${id}`, updatedOffre);
}

// hedhy l methode ll postuler tjibli les informations ta3 utilisateur eli besh ypostuler
postuler(id: string): Observable<string> {
  const token = sessionStorage.getItem('auth-user');
  let tokenObj : any; 
  if (token !== null) {
    tokenObj =  JSON.parse(token);
  }
  const authToken = tokenObj.token;

  const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + authToken
    })
  };

  return this.http.get<string>(API_URL  + 'postuler/'+ id, httpOptions);
}
likeOffre(offreId: string): Observable<any> {
  
  return this.http.put(`${API_URL}${offreId}/like`, null);

}
}