import { Component, OnInit } from '@angular/core';
import { News } from '../_servicesNews/News';
import { ServicesNewsService } from '../_servicesNews/-services-news.service';
import { offre } from '../_servicesOffres/offre';
import { OffreService } from '../_servicesOffres/offre.service';
import { Router } from '@angular/router';

import Swal from 'sweetalert2';


@Component({
  selector: 'app-acceuil',
  templateUrl: './acceuil.component.html',
  styleUrls: ['./acceuil.component.css']
})
export class AcceuilComponent implements OnInit {
  showMenu: boolean = false;
  newsList: News[] = [];
  newsListoffre: offre[] = [];
  selectedNews: News | null = null;
  selectedNewsoffre: offre | null = null;
  currentPage = 1;
  itemsPerPage = 3;
  selectedSlide = 0;
  constructor(private newsService: ServicesNewsService  , 
    private offreService :OffreService ,private router : Router) {}
  get pagedNewsList(): News[] {
    const startIndex = (this.currentPage - 1) * this.itemsPerPage;
    const endIndex = startIndex + this.itemsPerPage;
    return this.newsList.slice(startIndex, endIndex);
    
  }
  changeSlide(direction: string): void {
    const slides = document.querySelectorAll('.slide');
    const totalSlides = slides.length;
    const prevSlide = this.selectedSlide;

    if (direction === 'prev') {
      this.selectedSlide = (this.selectedSlide - 1 + totalSlides) % totalSlides;
    } else if (direction === 'next') {
      this.selectedSlide = (this.selectedSlide + 1) % totalSlides;
    }

    slides[prevSlide].classList.remove('active');
    slides[this.selectedSlide].classList.add('active');
  }
  get pagedNewsListoffre(): offre[] {
    const startIndex = (this.currentPage - 1) * this.itemsPerPage;
    const endIndex = startIndex + this.itemsPerPage;
    return this.newsListoffre.slice(startIndex, endIndex);
  }
  

  
  ngOnInit(): void {
    this.getAllNews();
    this.getOffresExterne();
    // Get the slides, buttons, and radio buttons
    const slides = document.querySelectorAll('.slide');
    const prevBtn = document.getElementById('prevBtn') as HTMLElement;
    const nextBtn = document.getElementById('nextBtn') as HTMLElement;
    const radioButtons = document.querySelectorAll<HTMLInputElement>('input[name="slide"]');

    let currentSlide = 0;

    // Display the current slide
    const showSlide = (slideIndex: number) => {
      slides.forEach((slide) => {
        slide.classList.remove('active');
      });
      slides[slideIndex].classList.add('active');
    };

    // Event listener for previous button
    prevBtn.addEventListener('click', () => {
      currentSlide--;
      if (currentSlide < 0) {
        currentSlide = slides.length - 1;
      }
      showSlide(currentSlide);
      updateRadioButtons();
    });

    // Event listener for next button
    nextBtn.addEventListener('click', () => {
      currentSlide++;
      if (currentSlide === slides.length) {
        currentSlide = 0;
      }
      showSlide(currentSlide);
      updateRadioButtons();
    });

    // Event listener for radio buttons
    radioButtons.forEach((radio) => {
      radio.addEventListener('change', () => {
        currentSlide = parseInt(radio.value);
        showSlide(currentSlide);
      });
    });

    // Update radio buttons based on current slide
    const updateRadioButtons = () => {
      radioButtons.forEach((radio) => {
        radio.checked = parseInt(radio.value) === currentSlide;
      });
    };

    // Show the initial slide
    showSlide(currentSlide);
  }
  getAllNews() {
    this.newsService.getAllNews().subscribe((data: News[]) => {
      this.newsList = data;

      // Sort the news list in descending order by datepub
      this.newsList.sort((a, b) => new Date(b.datepub).getTime() - new Date(a.datepub).getTime());

      // Show the first news item by default
      this.selectedNews = this.newsList[0];
    });
  }
  getOffresExterne() {
    this.offreService.getOffresExterne().subscribe((data: offre[]) => {
      this.newsListoffre = data;

      // Sort the news list in descending order by datepub
      this.newsListoffre.sort((a, b) => new Date(b.date_publication).getTime() - new Date(a.date_publication).getTime());

      // Show the first news item by default
      this.selectedNewsoffre = this.newsListoffre[0];
    });
  }



  detail(id:string){
    this.router.navigate(['detail',id]);
   }
   likeOffer(offreId: string): void {
    this.offreService.likeOffre(offreId).subscribe(
      (_response: any) => {
        Swal.fire({
          icon: 'success',
          title: 'Offre aimée avec succès',
          showConfirmButton: false,
          timer: 1500
        });
        // Additional logic if needed
      },
      (error: any) => {
        Swal.fire({
          icon: 'success',
          title: 'Offre aimée avec succès',
          showConfirmButton: false,
          timer: 1500
        });
        console.error('Failed to like offer:', error);
        // Additional error handling if needed
      }
    );
  }
  showSingleNews(id: string) {
    this.router.navigate(['detailnews',id]);
    this.newsService.getNewsById(id).subscribe((data: News) => {
      if (data !== undefined) {
        this.selectedNews = data;
      }
    });
  }
  
}
