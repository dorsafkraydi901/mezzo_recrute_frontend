import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProfileComponent } from './profile/profile.component';
import { AcceuilComponent } from './acceuil/acceuil.component';
import { LoginComponent } from './login/login.component';
import { RegistreComponent } from './registre/registre.component';
import { LoginEmpComponent } from './login-emp/login-emp.component';
import { AddoffreComponent } from './_offre/addoffre/addoffre.component';
import { FormsModule } from '@angular/forms';
import { EditpComponent } from './editp/editp.component';
import { ShowComponent } from './_offre/show/show.component';
import { EditoComponent } from './_offre/edito/edito.component';
import { DetailoffreComponent } from './_offre/detailoffre/detailoffre.component';
import { AddcandidatureComponent } from './_candidature/addcandidature/addcandidature.component';
import { ShowcandidatureComponent } from './_candidature/showcandidature/showcandidature.component';

// demande importations
import { AddDemandeComponent } from './demande/add-demande/add-demande.component';
import { DemandeDetailsComponent } from './demande/admin/demande-details/demande-details.component';
import { DemandedetailComponent } from './demande/demandedetail/demandedetail.component';
import { DemandeListComponent } from './demande/demande-list/demande-list.component';
import { ErrorComponent } from './error/error.component';
import { DemandesComponent } from './demande/demandes/demandes.component';
import { DemanValideComponent } from './demande/deman-valide/deman-valide.component';
import { DdemandeComponent } from './_offre/ddemande/ddemande.component';
import { DvheaderComponent } from './header/dvheader/dvheader.component';
import { ListeCandidatsComponent } from './_offre/liste-candidats/liste-candidats.component';
import { CandidateurdetailComponent } from './_offre/liste-candidats/candidateurdetail/candidateurdetail.component';
import { AdduseComponent } from './adduse/adduse.component';
import { SpinnerComponent } from './spinner/spinner.component';
import { CreatnewsComponent } from './news/creatnews/creatnews.component';
import { UpdatenewsComponent } from './news/updatenews/updatenews.component';
import { GetallComponent } from './news/getall/getall.component';
import { CandidatureAccepteComponent } from './candidature-accepte/candidature-accepte.component';
import { ContactComponent } from './contact/contact.component';
import { DetailsComponent } from './news/details/details.component';
import { ListeSpontaneComponent } from './liste-spontane/liste-spontane.component';
import { FooterComponent } from './footer/footer.component';

import { UpdateDemandeComponent } from './demande/update-demande/update-demande.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { EmailComponent } from './email/email.component';
import { CahngePasswordComponent } from './cahnge-password/cahnge-password.component';




// l pathet mta3 l paget lkol
const routes: Routes = [
    // w hedhy path ll list ta3 barsha demandes be3thinhom rd mokhtalfin
    { path: "demandeslist" , component:DemandeListComponent},
    // this path pour le responsable de department taffichilo les demande eli be3ethom houwa 
    {path : "demandes" , component:DemandesComponent},
  { path: "registre" , component:RegistreComponent },
  { path: "profile" , component:ProfileComponent },
  { path: "acceuil" , component:AcceuilComponent},
  { path: "login" , component:LoginComponent},
  { path: "adduser" , component:AdduseComponent},
  { path: "modif/:id" , component:EditpComponent},
  { path: "loginemp" , component:LoginEmpComponent},
  { path: "offres" , component:ShowComponent},
  { path: "modifier/:id" , component:EditoComponent},
  { path: "detail/:id" , component:DetailoffreComponent},
  { path: "postuler/:id" , component:AddcandidatureComponent},
  {path : "error" , component:ErrorComponent} ,
  { path: "adddemande" , component:AddDemandeComponent},
  { path: "detaile/:id" , component:DemandeDetailsComponent},
  { path: "detai/:id" , component:DemandedetailComponent},
  { path: "ddemande/:id" , component:DdemandeComponent},
  { path: "candidats/:id" , component:ListeCandidatsComponent},
  { path: "dvheader" , component:DvheaderComponent},
  { path: "canddetail/:id" , component:  CandidateurdetailComponent},
  { path: "demandevalide" , component:DemanValideComponent},
  { path: "mes-candidatures/:id" , component:ShowcandidatureComponent},
  { path: "spinner" , component:SpinnerComponent},
  { path: "addoffre/:demandeId" , component:AddoffreComponent},
  { path: "createnews" , component:CreatnewsComponent},
  { path: "news/:id" , component:UpdatenewsComponent},
  { path: "candidatutreaccepte" , component:CandidatureAccepteComponent}, 
  { path: 'n', redirectTo: '/news', pathMatch: 'full' },
  { path: 'news', component: GetallComponent },
  { path: 'Contact', component: ContactComponent },
  { path: 'footer', component: FooterComponent },
  { path: 'detailnews/:id', component: DetailsComponent },
  { path: 'listeSpontane', component: ListeSpontaneComponent },

  { path: 'changePassword/:id', component: CahngePasswordComponent },

  { path: 'editDemande/:id', component: UpdateDemandeComponent },
  
  

  {path: 'reset-password', component: ResetPasswordComponent},
  {path: 'email', component: EmailComponent},


  //t5ali l'acceuilComponent howa eli yji awel interface par defaut
  { path: '', redirectTo: 'acceuil', pathMatch: 'full' }
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes),
    FormsModule,],

  exports: [RouterModule]
})
export class AppRoutingModule { }
