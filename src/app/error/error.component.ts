import { Component, OnInit } from '@angular/core';
import {  ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-error',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.css']
})
export class ErrorComponent implements OnInit {
  @ViewChild('videoPlayer') videoPlayer!: ElementRef;

  constructor() { }

  ngOnInit(): void {
  }
  ngAfterViewInit() {
    this.videoPlayer.nativeElement.autoplay = true;
    this.videoPlayer.nativeElement.muted = true;
  }
}
