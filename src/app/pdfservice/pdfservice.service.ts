import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PdfserviceService {
  private apiEndpoint = 'http://localhost:8080/api/test/file';

  constructor(private http: HttpClient) { }

  getPdf(id: string): Observable<any> {
    const url = `${this.apiEndpoint}/id=${id}`;
    return this.http.get(url, { responseType: 'blob' });
  }

  uploadCv(userId: string, formData: FormData) {
    const url = `${this.apiEndpoint}/id=${userId}`;
    return this.http.post(url, formData);
  }
}
