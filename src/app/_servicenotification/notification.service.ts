import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Notification } from './Notification';


const API_URL = 'http://localhost:8080/notification/';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  constructor(private http: HttpClient) { }

  getNotificationsByUserID(userID: string): Observable<Notification[]> {
    const url = API_URL + userID;
    return this.http.get<Notification[]>(url);
  }

  getNotifications(): Observable<Notification[]> {
    const url = API_URL + 'notifications';
    return this.http.get<Notification[]>(url);
  }

  getAddNotificationsCount(): Observable<number> {
    const url = API_URL + 'notif/add/count';
    return this.http.get<number>(url);
  }



// zouz methode hedhom ta3 showvalidedemandesvalide w count
    getNotificationsvalide(): Observable<Notification[]> {
    const url = API_URL + 'notificationsvalideall';
    return this.http.get<Notification[]>(url);
  }
  getAddNotificationsCountvalide(): Observable<number> {
    const url = API_URL + 'notif/valideall/count';
    return this.http.get<number>(url);
  }

}
