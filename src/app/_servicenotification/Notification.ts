import { Utilisateur } from "../_servicesUser/utilisateur";

export class Notification {
    id!: string;
    content!: string;
    utilisateurTo!: Utilisateur;
    notificationType!: NotificationType;
  }
  export enum NotificationType {
    ADD = 'ADD',
    REMOVE = 'REMOVE',
    EDIT = 'EDIT',
    READ = 'READ'
  }