import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { News } from './News';

const API_URL = 'http://localhost:8080/api/test/news/';

@Injectable({
  providedIn: 'root'
})
export class ServicesNewsService {

  constructor(private http: HttpClient) { }
  getAllNews(): Observable<News[]> {
    return this.http.get<News[]>(API_URL);
  }
  getNewsById(id: string) {
    return this.http.get<News>(API_URL + "getnew/" + id);
  }
  updateNews(id: string, news: News): Observable<News> {
    return this.http.put<News>(API_URL + id, news);
  }

  deleteNews(id: string): Observable<any> {
    return this.http.delete(API_URL + id);
  }
  createNews(titre: string, content: string): Observable<any> {
    const token = sessionStorage.getItem('auth-user');
    let tokenObj: any;
    if (token !== null) {
      tokenObj = JSON.parse(token);
    }
    const authToken = tokenObj.token;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + authToken
      })
    };
    return this.http.post(API_URL + 'addnews', {
      titre,
      content
    }, httpOptions);
  }
  

}
