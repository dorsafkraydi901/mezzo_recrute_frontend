import { Utilisateur } from "../_servicesUser/utilisateur";
import { Photo } from "../photoservice/photo";

export class News {
    id!: string;
    titre!: string;
    datepub!: Date;
    content!: string;
    photos!: Photo;
    utilisateur!: Utilisateur;
  }