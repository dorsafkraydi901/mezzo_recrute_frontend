import { TestBed } from '@angular/core/testing';

import { ServicesNewsService } from './-services-news.service';

describe('ServicesNewsService', () => {
  let service: ServicesNewsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ServicesNewsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
