import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { StorageService } from '../_servicesUser/storage.service';
import { Utilisateur } from '../_servicesUser/utilisateur';
import { UserService } from '../_servicesUser/user.service';
import { FormGroup,FormControl, Validators, ValidationErrors } from '@angular/forms';
import Swal from 'sweetalert2';
import { AuthService } from '../_servicesUser/auth.service';
import { HttpClient } from '@angular/common/http';

import { ElementRef } from '@angular/core';
import { PhotoserviceService } from '../photoservice/photoservice.service';
import { PdfserviceService } from '../pdfservice/pdfservice.service';
import { AbstractControl, ValidatorFn } from '@angular/forms';


@Component({
  selector: 'app-editp',
  templateUrl: './editp.component.html',
  styleUrls: ['./editp.component.css']
})

export class EditpComponent implements OnInit {
  editform : any;
  
get username(){
  return this.editform.get('username'); 
}
get adresse(){
  return this.editform.get('adresse'); 
}
get phone(){
  return this.editform.get('phone'); 
}
get date_naiss(){
  return this.editform.get('date_naiss'); 
}
get email(){
  return this.editform.get('email'); 
}
get gender(){
  return this.editform.get('gender'); 
}
get password(){
  return this.editform.get('password'); 
}
file: File | null = null;
files: File | null = null;
  currentUser: any;
  utilisateur : Utilisateur=this.storageService.getUser();
  id ! : number ;
  isSuccessful = false;
  UserService: any;
  isLoggedIn = false;
  showCandidatBoard = false;
  roles: string[] = [];
  constructor(private storageService: StorageService,
    private service:UserService,
    private router: Router,private http: HttpClient,
     private  photoService: PhotoserviceService,
    private route : ActivatedRoute )
     { }

    ngOnInit(): void {
      console.log(this.utilisateur)
      this.editform = new FormGroup({
        username : new FormControl('',[Validators.required , Validators.pattern('^[a-zA-Z \-\']+')
         , Validators.minLength(3)] ),
     adresse : new FormControl('',[Validators.required ]),
        phone :new FormControl('' ,[Validators.required ]),
       date_naiss: new FormControl(null, [Validators.required, under18Validator()]),
        email :new FormControl('',[Validators.required ]),
        gender: new FormControl('', [Validators.required, genderValidator]),
        password :new FormControl('',[Validators.minLength(6),Validators.required ]),
      })
      function genderValidator(control: AbstractControl): ValidationErrors | null {
        const validValues = ['Homme', 'Femme'];
        const inputValue = control.value;
      
        if (!validValues.includes(inputValue)) {
          return { invalidGender: true };
        }
      
        return null;
      }
      
      //1--> hedhi tchoufelna l utilisateur connecté wala le o tchouf les information te3o zeda valide wale
      this.isLoggedIn = this.storageService.isLoggedIn();

//  2--> keno connecté tjibo b role te3o
      if (this.isLoggedIn) {
        const user = this.storageService.getUser();
        this.roles = user.roles;
        this.showCandidatBoard = this.roles.includes('ROLE_CANDIDAT');
      }

// 3-->ken mouch connecté t5arjelna error
      if (!this.isLoggedIn){
        this.router.navigate(['error']);
      }
      if (!this.showCandidatBoard ){ this.router.navigate(['error']);
    }

    
// 4-->tjibelna les donne ta3 utilisateur el correspond lel id connecté
this.currentUser = this.storageService.getUser();
this.id = this.route.snapshot.params['id'];
this.service.findUserById(this.id.toString()).subscribe(data => {
  this.utilisateur = data;

});
     }

  //  5--> ken mat7ebch tmodifi traj3ek lel profil  
     annuler() {
      this.router.navigate(['profile']);
    }

    // 6--> ken tmodifa traj3ek lel login bech t3awed todkhel b les info jdod
    modifierUser(){
      this.service.updateUser(this.utilisateur).subscribe(data=>{
    
        this.onSubmite();
        this.cvupdate();
        this.storageService.clean();
        Swal.fire({
          position: 'top',
          icon: 'success',
          text: "Votre compte a été mis à jour. Veuillez vous reconnecter.",
          showConfirmButton: false,
          timer: 2000
        })
        this.router.navigateByUrl('/login');
      }
      );
    }
    //pour la button logout
    logout(): void {
      if (this.router.url === '/acceuil') {
        window.location.reload();
      } else {
        this.router.navigate(['acceuil']);
      }
      this.storageService.signOut();
    }

    onFileSelected(event: any) {
      this.file = event.target.files[0];
    }

    onCvSelected(event: any) {
      this.files = event.target.files[0];
    }
    onSubmite() {
      if (!this.file) {
        return;
      }
    
      const allowedTypes = ['image/jpeg', 'image/png', 'image/gif'];
      if (!allowedTypes.includes(this.file.type ?? '')) {
        console.error('Invalid file type. Only JPEG, PNG, and GIF images are allowed.');
        return;
      }
    
      // Rest of the code to upload the image
      const user = this.storageService.getUser();
      this.service.findUserById(user.id.toString()).subscribe(data => {
        this.utilisateur = data;
      });
    
      const formData = new FormData();
      formData.append('image', this.file);
    
      this.photoService.uploadImage(user.id, formData).subscribe(
        () => {
          console.log('Image uploaded successfully');
        },
        error => {
          console.error('Error uploading image:', error);
        }
      );
    }
  
    cvupdate(){
      if (!this.files) {
        return;
      }
      const id = this.route.snapshot.params['id'];
      const formData = new FormData();
      formData.append('file', this.files);
      this.photoService.uploadCv(id, formData).subscribe(
        () => {
          console.log('cv uploaded successfully');
        },
        error => {
          console.error('Error uploading cv:', error);
        }
      );
    }
    
    isUnderAge(): boolean {
      const now = new Date();
      const birthDate = new Date(this.date_naiss.value);
      let age = now.getFullYear() - birthDate.getFullYear();
      const monthDiff = now.getMonth() - birthDate.getMonth();
      if (monthDiff < 0 || (monthDiff === 0 && now.getDate() < birthDate.getDate())) {
        age--;
      }
      return age < 18 || age >= 45;
    }
    

    isValidGender(value: string): boolean {
      const validValues = ['homme', 'femme'];
      return validValues.includes(value.toLowerCase());
    }
    


  }

  export function under18Validator(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      const birthdate = new Date(control.value);
      const today = new Date();
      let age = today.getFullYear() - birthdate.getFullYear();
      const monthDiff = today.getMonth() - birthdate.getMonth();
      if (monthDiff < 0 || (monthDiff === 0 && today.getDate() < birthdate.getDate())) {
        age--;
      }
      return age < 18 || age > 45 ? { under18: true } : null;
    };



  }

  