import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup,FormControl, Validators } from '@angular/forms';
import { AuthService } from '../_servicesUser/auth.service';
import { StorageService } from '../_servicesUser/storage.service';
import { NgOneTapService } from 'ng-google-one-tap';
import { GoogleLoginProvider, SocialAuthService } from 'angularx-social-login';
import { PasswordResetServiceService } from '../_servicesUser/password-reset-service.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginform:any;
  form: any = {
    email: null,
    password: null
  };
  isLoggedIn = false;
  isLoginFailed = false;
  nom="";
  errorMessage = '';
  roles: string[] = [];

  constructor(private authService: AuthService, private storageService: StorageService,private authserviceg : SocialAuthService,
     private router: Router , private oneTap: NgOneTapService) { }

  ngOnInit(): void {
  this.loadlogin();
    this.loginform = new FormGroup({
      "email" :new FormControl('',[Validators.required , Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$") ]),
      "password"  :new FormControl('',[Validators.minLength(6),Validators.required ])
    })
    this.isLoggedIn = this.storageService.isLoggedIn();

    if (this.isLoggedIn) {
        this.router.navigate(['error']);

    }
  }
signInGoogleHandler(){
this.authserviceg.signIn(GoogleLoginProvider.PROVIDER_ID).then(
  (data)=>{
console.log(data)
});
}
loadlogin() : void {
  this.oneTap.tapInitialize(); //Initialize OneTap, At intial time you can pass config  like this.oneTap.tapInitialize(conif) here config is optional.
  this.oneTap.promtMoment.subscribe(res => {  // Subscribe the Tap Moment. following response options all have self explanatory. If you want more info pls refer official document below attached link.
     res.getDismissedReason(); 
     res.getMomentType();
     res.getNotDisplayedReason();
     res.getSkippedReason();
     res.isDismissedMoment();
     res.isDisplayed();
     res.isNotDisplayed();
     res.isSkippedMoment();
  });
  this.oneTap.oneTapCredentialResponse.subscribe(res => {
    const token = res.credential // After continue with one tap JWT credentials response.
   this.storageService.setToken(token);
   console.log(token)
   this.router.navigateByUrl('/acceuil')
   this.isLoggedIn = true;
  });

}
onSubmit(): void {
  const { email, password } = this.form;

  // Check if the form is empty
  if (!email || !password) {
    this.errorMessage = "email ou mot de passe  vide ";
    this.isLoginFailed = true;
    return;
  }

  this.authService.login(email, password).subscribe({
    next: data => {
      this.storageService.saveUser(data);
      this.router.navigateByUrl('/acceuil').then(this.reloadPage);
      this.isLoginFailed = false;
      this.isLoggedIn = true;
      this.roles = this.storageService.getUser().roles;
      this.reloadPage();
    },
    error: err => {
      console.error("Error response from server:", err); // Add this line for debugging
      if (err.status === 401) {
        // Email doesn't exist or password is incorrect
        this.errorMessage = "Email n'existe pas ou mot de passe incorrect";
      } else {
        // Other error
        this.errorMessage = "Échec de connexion.";
      }
      this.isLoginFailed = true;
    }
  });
}


  reloadPage(): void {
   window.location.reload();
  }
  get email(){
    return this.loginform.get('email'); 
  }
  get password(){
    return this.loginform.get('password'); 
  }

}






