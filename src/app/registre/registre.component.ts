
import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { AuthService } from '../_servicesUser/auth.service';
import { StorageService } from '../_servicesUser/storage.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AbstractControl, ValidatorFn } from '@angular/forms';

@Component({
  selector: 'app-registre',
  templateUrl: './registre.component.html',
  styleUrls: ['./registre.component.css']
})
export class RegistreComponent {
  registreform: any;

  form: any = {
    username: null,
    email: null,
    password: null,
    adresse: null,
    phone: null,
    date_naiss: null,
    gender: null,
  };
  isSuccessful = false;
  isSignUpFailed = false;
  errorMessage = '';
  isLoggedIn = false;

  constructor(private authService: AuthService, private router: Router, 
    private storageService: StorageService) { }
  ngOnInit(): void {
    // validation for the form 
    this.registreform = new FormGroup({
      "username": new FormControl(null, [Validators.required,Validators.minLength(3),Validators.maxLength(10)]),
      "adresse": new FormControl(null, [Validators.required, Validators.pattern(/^[a-zA-Z\s]*$/)]),
      "email": new FormControl(null, [Validators.required, Validators.email]),
      "password": new FormControl(null, [Validators.minLength(6), Validators.required]),
      "phone": new FormControl(null, [Validators.required, Validators.pattern("^[0-9]+")]),
      "date_naiss": new FormControl(null, [Validators.required, under18Validator()]),
      "gender": new FormControl(null, [Validators.required]),


    })
    this.isLoggedIn = this.storageService.isLoggedIn();

    if (this.isLoggedIn) {
        this.router.navigate(['error']);

    }

  }
  get username() {
    return this.registreform.get('username');
  }
  get adresse() {
    return this.registreform.get('adresse');
  }
  
  get email() {
    return this.registreform.get('email');
  }
  
  get password() {
    return this.registreform.get('password');
  }
    
  get phone() {
    return this.registreform.get('phone');
  }
    
  get date_naiss() {
    return this.registreform.get('date_naiss');
  }
  get gender() {
    return this.registreform.get('gender');
  }






  onSubmit(): void {

    const { username, email, password, adresse, phone, date_naiss,gender } = this.form;

    this.authService.register(username, email, password, adresse, phone, date_naiss,gender).subscribe({
      next: data => {
        console.log(data);
        if (this.isSuccessful = true)
          Swal.fire({
            position: 'top',
            icon: 'success',
            confirmButtonColor: '#25377A',
            title: "Félicitation! Votre inscription est réussie.",
            showConfirmButton: true,
          })
        this.router.navigateByUrl('/login');
        this.isSignUpFailed = false;
      },
      error: err => {
        this.errorMessage = "Échec de l'inscription.";

        this.isSignUpFailed = true;
      }
    });
  }
  isUnderAge(): boolean {
    const now = new Date();
    const birthDate = new Date(this.date_naiss.value);
    let age = now.getFullYear() - birthDate.getFullYear();
    const monthDiff = now.getMonth() - birthDate.getMonth();
    if (monthDiff < 0 || (monthDiff === 0 && now.getDate() < birthDate.getDate())) {
      age--;
    }
    return age < 18 || age >= 45;
  }
}

export function under18Validator(): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } | null => {
    const birthdate = new Date(control.value);
    const today = new Date();
    let age = today.getFullYear() - birthdate.getFullYear();
    const monthDiff = today.getMonth() - birthdate.getMonth();
    if (monthDiff < 0 || (monthDiff === 0 && today.getDate() < birthdate.getDate())) {
      age--;
    }
    return age < 18 || age > 45 ? { under18: true } : null;
  };
}
